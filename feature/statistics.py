from collections import OrderedDict
from sklearn.preprocessing import binarize
import numpy as np
import pandas as pd

from . bag_of_app import *

device_with_event = matrix_ravel(index_event.sum(axis=1) > 0)
active_event = index_event[device_with_event]
active_event_with_app = active_event[:, matrix_ravel(event_installed_app_matrix.sum(axis=1)>0)]
active_event_count = matrix_ravel(active_event.sum(axis=1))
active_event_with_app_count = matrix_ravel(active_event_with_app.sum(axis=1))
active_event_normalized = sp.diags(1/active_event_count).dot(active_event)
active_event_normalized_2 = sp.diags(1/active_event_with_app_count).dot(active_event)


feats = OrderedDict()

# app statistics
feats['n_event'] = active_event_count
feats['n_event_with_no_app'] = active_event_count-active_event_with_app_count
feats['ratio_event_with_no_app'] = feats['n_event_with_no_app']/feats['n_event']
feats['n_app_installed_in_any_event'] = matrix_ravel(binarize(installed_app_count[device_with_event]).sum(axis=1))
feats['n_active_in_any_event'] = matrix_ravel(binarize(active_app_count[device_with_event]).sum(axis=1))
feats['mean_installed_app_count_in_event'] = matrix_ravel(active_event_normalized.dot(event_installed_app_matrix.sum(axis=1)))
feats['mean_active_app_count_in_event'] = matrix_ravel(active_event_normalized.dot(event_active_app_matrix.sum(axis=1)))
feats['mean_ratio_of_active_app_in_event'] = active_event_normalized.dot(np.nan_to_num(matrix_ravel(event_active_app_matrix.sum(axis=1)/event_installed_app_matrix.sum(axis=1))))
feats['n_event_with_no_active_app'] = matrix_ravel(active_event.dot(event_active_app_matrix.sum(axis=1)==0))
feats['ratio_event_with_no_active_app'] = feats['n_event_with_no_active_app']/feats['n_event']
app_ratio_of_installed = active_event_normalized_2.dot(event_installed_app_matrix)
app_ratio_square_of_installed = app_ratio_of_installed.copy()
app_ratio_square_of_installed.data = app_ratio_square_of_installed.data**2
feats['mean_app_ratio_of_installed'] = matrix_ravel(app_ratio_of_installed.sum(axis=1))/feats['n_app_installed_in_any_event']
feats['std_app_ratio_of_installed'] = np.sqrt(matrix_ravel(app_ratio_square_of_installed.sum(axis=1))/feats['n_app_installed_in_any_event']-feats['mean_app_ratio_of_installed']**2)

# location feature
event_with_valid_location = ~((events.longitude.values==0) & (events.latitude.values == 0))
active_event_with_valid_location = active_event[:, event_with_valid_location]
mean_loc = sp.diags(1/matrix_ravel(active_event_with_valid_location.sum(axis=1))).\
    dot(active_event_with_valid_location.\
        dot(events[['longitude', 'latitude']].values[event_with_valid_location]))
# sqrt will give strange result due to  precision; use var
var_loc = sp.diags(1/matrix_ravel(active_event_with_valid_location.sum(axis=1))).\
    dot(active_event_with_valid_location.\
        dot(events[['longitude', 'latitude']].values[event_with_valid_location]**2))\
    -mean_loc**2
feats['mean_longitude'] = mean_loc[:, 0]
feats['mean_latitude'] = mean_loc[:, 1]
feats['var_longitude'] = var_loc[:, 0]
feats['var_latitude'] = var_loc[:, 1]
feats['ratio_of_event_without_valid_location'] = matrix_ravel(active_event_with_valid_location.sum(axis=1))/feats['n_event']

feats_df = pd.DataFrame(feats)

def get_full_feats_df(feats_df):
    res = np.empty((index_event.shape[0], feats_df.shape[1])) * np.nan
    res[device_with_event] = feats_df.values
    return pd.DataFrame(res, columns=feats_df.columns)
