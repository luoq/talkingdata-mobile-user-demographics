from .bag_of_app import installed_app_count, device_with_installed_app
from sklearn.preprocessing import binarize
from sklearn.decomposition import NMF, TruncatedSVD
from datatrek.util import with_cache
from config import config
import os

__all__ = ['installed_app_svd_200','installed_app_svd_500', 'installed_app_nmf_200', 'installed_app_nmf_500']
data_root = config['data_root']
data_subroot = 'feature'
os.makedirs(os.path.join(data_root, data_subroot), exist_ok=True)

installed_app = binarize(installed_app_count[device_with_installed_app])

def gen_embedding(model):
    X = model.fit_transform(installed_app)
    return model, X

def gen_embedding_cached(name, model):
    return with_cache(os.path.join(data_root, data_subroot, name+'.pickle'))(lambda : gen_embedding(model))()

_, installed_app_svd_200 = gen_embedding_cached('installed_app_svd_200', TruncatedSVD(n_components=200, random_state=32))
_, installed_app_svd_500 = gen_embedding_cached('installed_app_svd_500', TruncatedSVD(n_components=500, random_state=33))
_, installed_app_nmf_200 = gen_embedding_cached('installed_app_nmf_200', NMF(n_components=200, random_state=34, shuffle=True, max_iter=20))
_, installed_app_nmf_500 = gen_embedding_cached('installed_app_nmf_500', NMF(n_components=500, random_state=35, shuffle=True, max_iter=20))