from sklearn.model_selection import StratifiedKFold
import scipy.sparse as sp

from data.remapped import *
from util import build_onehot_matrix_with_encoder, matrix_ravel, build_csr_matrix


# phone brand and phone model
phone_brand = build_onehot_matrix_with_encoder(gender_age.phone_brand_int, phone_brand_encoder)
phone_brand_model = build_onehot_matrix_with_encoder(gender_age.phone_brand_model_int, phone_brand_model_encoder)

# bag of app
index_device_matrix = build_onehot_matrix_with_encoder(gender_age.device_id, device_encoder)
index_event = index_device_matrix.dot(device_event_matrix)
index_event_with_app = index_event[:, matrix_ravel(event_installed_app_matrix.sum(axis=1) > 0)]
event_count = matrix_ravel(index_event.sum(axis=1))
event_with_app_count = matrix_ravel(index_event_with_app.sum(axis=1))

installed_app_count = index_event.dot(event_installed_app_matrix)
active_app_count = index_event.dot(event_active_app_matrix)

installed_app_ratio = sp.diags(1/event_count).dot(installed_app_count)
active_app_ratio = sp.diags(1/event_count).dot(active_app_count)
installed_app_label = installed_app_ratio.dot(app_label_matrix)
active_app_label = active_app_ratio.dot(app_label_matrix)


installed_app_ratio_2 = sp.diags(1 / event_with_app_count).dot(installed_app_count)
active_app_ratio_2 = sp.diags(1 / event_with_app_count).dot(active_app_count)
installed_app_label_2 = installed_app_ratio_2.dot(app_label_matrix)
active_app_label_2 = active_app_ratio_2.dot(app_label_matrix)
installed_app_category_2 = installed_app_label_2.dot(label_category_matrix)
active_app_category_2 = active_app_label_2.dot(label_category_matrix)

# split app bag by hour
events['hour'] = events['timestamp'].apply(lambda x: x.hour)
event_hour = build_csr_matrix(events.event_id, events.hour, shape=(len(event_encoder.classes_), 24)).tocsc()
def split_by_hour(device_event, event_hour, event_bag):
    res = []
    for i in range(event_hour.shape[1]):
        mask = event_hour[:, i].indices
        res.append(device_event[:, mask].dot(event_bag[mask, ]))
    return sp.hstack(res).tocsr()

installed_app_count_by_hour = split_by_hour(index_event, event_hour, event_installed_app_matrix)
active_app_count_by_hour = split_by_hour(index_event, event_hour, event_active_app_matrix)

# sample filter
device_with_installed_app = matrix_ravel(installed_app_count.sum(axis=1)) > 0
device_with_event = matrix_ravel(index_event.sum(axis=1) > 0)