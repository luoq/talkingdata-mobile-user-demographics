import numpy as np
np.random.seed(42)

from keras.callbacks import ModelCheckpoint
from sklearn.feature_selection import VarianceThreshold
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import Binarizer, LabelBinarizer
import os
import pandas as pd


from feature.bag_of_app import *

groups = group_encoder.classes_
train_mask = gender_age.is_train.values
n_sample = gender_age.shape[0]
n_train = train_mask.sum()
n_test = len(train_mask) - n_train
y_train = gender_age.group.values[train_mask]

X = sp.hstack((phone_brand, phone_brand_model, installed_app_count, installed_app_label)).tocsr()

transformer = make_pipeline(VarianceThreshold(), Binarizer())
X = transformer.fit_transform(X)

X_train = X[train_mask]
X_test = X[~train_mask]

X1, X2, y1, y2 = train_test_split(X_train, y_train, test_size=0.1, stratify=y_train, random_state=0)
classes = np.unique(y_train)

group_binaizer = LabelBinarizer().fit(classes)

from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout
from keras.optimizers import Nadam, Adam


def batch_generator(X, batch_size, y=None, shuffle=False, seed=0):
    number_of_batches = np.ceil(X.shape[0]/batch_size)
    counter = 0
    sample_index = np.arange(X.shape[0])
    if shuffle:
        rng = np.random.RandomState(seed)
        rng.shuffle(sample_index)
    while True:
        batch_index = sample_index[batch_size*counter:batch_size*(counter+1)]
        X_batch = X[batch_index,:].toarray()
        if y is not None:
            y_batch = y[batch_index]
        counter += 1
        if y is not None:
            yield X_batch, y_batch
        else:
            yield X_batch
        if (counter == number_of_batches):
            if shuffle:
                rng.shuffle(sample_index)
            counter = 0


model = Sequential([
    Dense(500, input_dim=X_train.shape[1], init='glorot_normal'),
    Dropout(0.5),
    Activation('relu'),
    Dense(500, input_dim=X_train.shape[1], init='glorot_normal'),
    Dropout(0.5),
    Activation('relu'),
    Dense(len(classes), init='glorot_normal'),
    Activation('softmax'),
])

model.compile(
    optimizer=Nadam(lr=1e-4),
    loss='categorical_crossentropy',
    metrics=['accuracy'],
)
train_generator = batch_generator(X1, y=group_binaizer.transform(y1), batch_size=200, shuffle=True)
val_generator = batch_generator(X2, y=group_binaizer.transform(y2), batch_size=200, shuffle=True)
test_generator = batch_generator(X_test, batch_size=200, shuffle=False)


model_name = "keras_nn.1"
model_file = "data_files/model/{}.hdf5".format(model_name)
submission_file = "data_files/model/{}.submission.csv".format(model_name)
if not os.path.exists(model_file):
    model_checkpoint = ModelCheckpoint(model_file, monitor='val_loss', save_best_only=True)
    model.fit_generator(generator=train_generator,
                              samples_per_epoch=X1.shape[0],
                              validation_data=val_generator,
                              nb_val_samples=X2.shape[0],
                              nb_epoch=10,
                              callbacks=[model_checkpoint]
                              )
model.load_weights(model_file)

prob = model.predict_generator(test_generator, val_samples=X_test.shape[0])

test_set_original_device_id = device_encoder.inverse_transform(gender_age[~gender_age.is_train].device_id)
pd.DataFrame(prob, index=test_set_original_device_id, columns=group_binaizer.classes_).to_csv(submission_file, index_label='device_id')