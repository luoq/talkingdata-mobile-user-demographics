import os

import pandas as pd
from datatrek.util import with_cache
from sklearn.preprocessing import LabelEncoder

from config import config

data_root = config['data_root']
data_subroot = 'remapped'
os.makedirs(os.path.join(data_root, data_subroot), exist_ok=True)

from data.original import *
from util import CSRMatrixBuilder, build_csr_matrix, build_csr_matrix_with_encoder


@with_cache(os.path.join(data_root, data_subroot, 'event_app_matrix.pickle'))
def gen_event_app_matrix():
    # encode app_id
    # 113211 apps in app_labels, only 19237 app_events; we align to app_events here
    # use app_labels for
    app_encoder = LabelEncoder().fit(app_labels.app_id)
    app_events['app_id'] = app_encoder.transform(app_events.app_id)

    # event_id is continuous from 1 to 3252950 in events, for consistency we encode it any way
    event_encoder = LabelEncoder().fit(events.event_id)
    app_events['event_id'] = event_encoder.transform(app_events.event_id)

    installed_app_builder = CSRMatrixBuilder((len(event_encoder.classes_), len(app_encoder.classes_)))
    active_app_builder = CSRMatrixBuilder((len(event_encoder.classes_), len(app_encoder.classes_)))
    for k, df in app_events.groupby('event_id'):
        installed = df['app_id'].values
        active = installed[df['is_active'].values == 1]
        installed_app_builder.add_row(k, installed.tolist())
        active_app_builder.add_row(k, active.tolist())
    installed_app = installed_app_builder.get()
    active_app = active_app_builder.get()
    return event_encoder, app_encoder, installed_app, active_app


event_encoder, app_encoder, event_installed_app_matrix, event_active_app_matrix = gen_event_app_matrix()


@with_cache(os.path.join(data_root, data_subroot, 'device_event.pickle'))
def gen_device_event_matrix():
    # device_id in gender_age and events has no subset relation
    device_encoder = LabelEncoder().fit(gender_age_train.device_id.tolist()
                                        + gender_age_test.device_id.tolist() + events.device_id.tolist())
    events['device_id'] = device_encoder.transform(events['device_id'])
    events['event_id'] = event_encoder.transform(events['event_id'])
    device_event = build_csr_matrix(events['device_id'], events['event_id'],
                                    shape=(len(device_encoder.classes_), len(event_encoder.classes_)))
    return device_encoder, device_event, events


device_encoder, device_event_matrix, events = gen_device_event_matrix()


@with_cache(os.path.join(data_root, data_subroot, 'app_label.pickle'))
def gen_app_label_matrix():
    # label_categories.label_id containes app_label.label_id
    label_encoder = LabelEncoder().fit(label_categories.label_id)
    app_labels['app_id'] = app_encoder.transform(app_labels['app_id'])
    app_labels['label_id'] = label_encoder.transform(app_labels['label_id'])
    app_label = build_csr_matrix(app_labels['app_id'], app_labels['label_id'],
                                 shape=(len(app_encoder.classes_), len(label_encoder.classes_)))
    label_categories['label_id'] = label_encoder.transform(label_categories['label_id'])
    return label_encoder, app_label, label_categories


label_encoder, app_label_matrix, label_categories = gen_app_label_matrix()


@with_cache(os.path.join(data_root, data_subroot, 'label_category.pickle'))
def gen_label_category_matrix():
    # 3 nan in category
    label_categories.fillna('null', inplace=True)
    # every label_id has one category
    # 26 labels has unknown category; other cateogries has at most 3 label
    label_categories['label_id'] = label_encoder.fit_transform(label_categories['label_id'])
    category_encoder = LabelEncoder()
    label_categories['category'] = category_encoder.fit_transform(label_categories['category'])
    label_category = build_csr_matrix_with_encoder(label_categories['label_id'], label_categories['category'],
                                                   label_encoder, category_encoder)
    return category_encoder, label_category


category_encoder, label_category_matrix = gen_label_category_matrix()


@with_cache(os.path.join(data_root, data_subroot, 'gender_age.pickle'))
def gen_gender_age():
    # model has no independent meaning
    phone_brand_device_model['phone_brand_model'] = list(
        zip(phone_brand_device_model.phone_brand, phone_brand_device_model.device_model))

    group_encoder = LabelEncoder()
    gender_age_train['group_int'] = group_encoder.fit_transform(gender_age_train['group'])

    gender_encoder = LabelEncoder()
    gender_age_train['gender_int'] = gender_encoder.fit_transform(gender_age_train['gender'])

    # concat train, test for easy data transform
    gender_age = pd.concat((gender_age_train, gender_age_test), names=['is_train'],
                           keys=[True, False]).reset_index().drop(['level_1'], axis=1)

    # no need to store phone_brand_device_model separately
    phone_brand_device_model.drop_duplicates('device_id', inplace=True)  # some device_id occurs multi times
    gender_age = gender_age.merge(phone_brand_device_model, on='device_id')

    # encode brand and (brand, model)
    brand_encoder = LabelEncoder().fit(phone_brand_device_model.phone_brand)
    brand_model_encoder = LabelEncoder().fit(phone_brand_device_model.phone_brand_model)
    gender_age['phone_brand_int'] = brand_encoder.transform(gender_age.phone_brand)
    gender_age['phone_brand_model_int'] = brand_model_encoder.transform(gender_age.phone_brand_model)

    gender_age['device_id'] = device_encoder.transform(gender_age['device_id'])
    return gender_age, brand_encoder, brand_model_encoder, gender_encoder, group_encoder


gender_age, phone_brand_encoder, phone_brand_model_encoder, gender_encoder, group_encoder = gen_gender_age()

__all__ = ['device_encoder', 'event_encoder', 'app_encoder', 'label_encoder', 'category_encoder', 'phone_brand_encoder',
           'phone_brand_model_encoder',
           'gender_encoder', 'group_encoder',
           'gender_age', 'events', 'label_category_matrix',
           'device_event_matrix', 'event_installed_app_matrix', 'event_active_app_matrix', 'app_label_matrix']
