import pandas as pd
from config import config
import os
from datatrek.util import with_cache

data_root = config['data_root']

def load_file_with_cache(name, reader, f):
    return with_cache(os.path.join(data_root, 'original' ,name + '.pickle'))(lambda: reader(os.path.join(data_root, 'original', f)))()
names1 = ['app_events', 'app_labels', 'gender_age_test', 'gender_age_train', 'label_categories', 'phone_brand_device_model']
__all__ = names1 + ['events']
for name in names1:
    globals()[name] = load_file_with_cache(name, pd.read_csv, name+'.csv')
events = load_file_with_cache('events', lambda x: pd.read_csv(x, parse_dates=['timestamp']), 'events.csv')
