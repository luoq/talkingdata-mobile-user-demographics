from sklearn.base import BaseEstimator, ClassifierMixin
import numpy as np
from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
from keras.callbacks import Callback


def batch_generator(X, batch_size, y=None, shuffle=False, rng=None):
    number_of_batches = np.ceil(X.shape[0] / batch_size)
    counter = 0
    sample_index = np.arange(X.shape[0])
    if shuffle:
        rng.shuffle(sample_index)
    while True:
        batch_index = sample_index[batch_size * counter:batch_size * (counter + 1)]
        X_batch = X[batch_index, :].toarray()
        if y is not None:
            y_batch = y[batch_index]
        counter += 1
        if y is not None:
            yield X_batch, y_batch
        else:
            yield X_batch
        if (counter == number_of_batches):
            if shuffle:
                rng.shuffle(sample_index)
            counter = 0


class BestKeeper(Callback):
    def __init__(self, monitor='val_loss', lower_is_better=True, patience=np.inf):
        self.monitor = monitor
        self.lower_is_better = lower_is_better
        self.patience = patience

    def on_train_begin(self, logs={}):
        self.best_weights_ = None
        self.best_score_ = None
        self.best_epoch_ = None
        self.is_better_ = np.less if self.lower_is_better else np.greater
        self.wait_ = 0

    def on_epoch_end(self, epoch, logs={}):
        current_score = logs.get(self.monitor)
        if self.best_score_ is None or self.is_better_(current_score, self.best_score_):
            self.best_epoch_ = epoch
            self.best_score_ = current_score
            self.best_weights_ = self.model.get_weights()
            self.wait_ = 0
        else:
            if self.wait_ >= self.patience:
                self.model.stop_training = True
            self.wait_ += 1


class KerasClassifier(BaseEstimator, ClassifierMixin):
    def __init__(self, model_generator, seed=42, batch_size=200, shuffle=True, validation_ratio=0.1, max_iter=20,
                 patience=np.inf, repeat=1, verbose=0, method='average'):
        if method not in ['average', 'best']:
            raise RuntimeError('invalid parameter method')
        self.seed = seed
        self.model_generator = model_generator
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.validation_ratio = validation_ratio
        self.max_iter = max_iter
        self.patience = patience
        self.repeat = repeat
        self.verbose = verbose
        self.method = method

        self.rng_ = np.random.RandomState(seed)
        self.seed_ = seed * 3 + 1

    def _init_model(self, input_dim, seed):
        np.random.seed(seed)
        return self.model_generator(input_dim)

    def _fit(self, model, X1, X2, y1, y2):
        data_train = batch_generator(X1, y=y1, batch_size=self.batch_size, shuffle=self.shuffle, rng=self.rng_)
        data_val = batch_generator(X2, y=y2, batch_size=self.batch_size, shuffle=False)
        best_keeper = BestKeeper(patience=self.patience)
        model.fit_generator(generator=data_train, samples_per_epoch=X1.shape[0],
                            validation_data=data_val, nb_val_samples=X2.shape[0],
                            nb_epoch=self.max_iter,
                            callbacks=[best_keeper],
                            verbose=self.verbose)
        model.set_weights(best_keeper.best_weights_)

        return best_keeper.best_epoch_, best_keeper.best_score_, model

    def fit(self, X, y):
        self.y_binarizer_ = LabelBinarizer()
        y_ = y
        y = self.y_binarizer_.fit_transform(y)
        self.classes_ = self.y_binarizer_.classes_

        X1, X2, y1, y2 = train_test_split(X, y, test_size=self.validation_ratio, random_state=self.rng_, stratify=y_)
        self.model_results_ = []
        for i in range(self.repeat):
            if self.verbose == 1:
                print('start training model {}/{}'.format(i + 1, self.repeat))
            model = self._init_model(input_dim=X.shape[1], seed=self.seed_)
            self.seed_ = self.seed_ * 3 + 1
            self.model_results_.append(self._fit(model, X1, X2, y1, y2))

        if self.method == 'best':
            self.model_results_.sort(key=lambda x: x[1])
            self.model_results_ = [self.model_results_[0]]

        return self

    def predict_proba(self, X):
        if self.method == 'average':
            proba = np.zeros((X.shape[0], len(self.classes_)))
            for _, _, model in self.model_results_:
                data = batch_generator(X, batch_size=self.batch_size, shuffle=False)
                proba += model.predict_generator(data, val_samples=X.shape[0])
            return proba / self.repeat
        elif self.method == 'best':
            model = self.model_results_[0][2]
            data = batch_generator(X, batch_size=self.batch_size, shuffle=False)
            return model.predict_generator(data, val_samples=X.shape[0])

    def predict(self, X):
        return self.classes_[self.predict_proba(X).argmax(axis=1)]
