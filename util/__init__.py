import scipy.sparse as sp
import numpy as np
from sklearn.preprocessing import LabelEncoder


class CSRMatrixBuilder:
    '''
    build csr matrix row by row
    '''

    def __init__(self, shape=None, dtype=int):
        self.shape = shape
        self.I = []
        self.J = []
        self.V = []

    def add_row(self, i, indices, data=None):
        if data is None:
            data = [1] * len(indices)
        # assert len(indices) == len(data)
        for j, v in zip(indices, data):
            self.add(i, j, v)

    def add(self, i, j, v=1):
        self.I.append(i)
        self.J.append(j)
        self.V.append(v)

    def add_bulk(self, I, J, V=None):
        I = list(I)
        J = list(J)
        if self.V is None:
            V = [1] * len(I)
        self.I.extend(I)
        self.J.extend(J)
        self.V.extend(V)

    def get(self):
        return sp.csr_matrix((self.V, (self.I, self.J)), shape=self.shape)


def build_csr_matrix(I, J, V=None, shape=None):
    if V is None:
        V = [1] * len(I)
    return sp.csr_matrix((V, (I, J)), shape=shape)


def encoder_size(encoder):
    return len(encoder.classes_)


def build_csr_matrix_with_encoder(I, J, row_encoder, col_encoder, V=None):
    return build_csr_matrix(I, J, V=V, shape=(encoder_size(row_encoder), encoder_size(col_encoder)))


def build_onehot_matrix_with_encoder(J, col_encoder):
    I = np.arange(len(J))
    return build_csr_matrix(I, J, shape=(len(J), encoder_size(col_encoder)))


def matrix_ravel(x):
    return np.array(x).ravel()


from sklearn.base import BaseEstimator, ClassifierMixin
import xgboost as xgb


class GBLinearClassifier(BaseEstimator, ClassifierMixin):
    def __init__(self, reg_lambda=0, reg_alpha=0, reg_lambda_bias=0, nthread=-1, learning_rate=0.1, n_estimators=100,
                 missing=None):
        self.reg_lambda = reg_lambda
        self.reg_alpha = reg_alpha
        self.reg_lambda_bias = reg_lambda_bias
        self.nthread = nthread
        self.learning_rate = learning_rate
        self.n_estimators = n_estimators
        self.missing = missing

    @staticmethod
    def _append_ones_at_right(X):
        '''
        workaround for https://github.com/dmlc/xgboost/issues/1091
        '''
        return sp.hstack((X, sp.csr_matrix(np.ones((X.shape[0], 1)))))

    def fit(self, X, y):
        self.missing_ = self.missing if self.missing is not None else np.nan
        self.target_encoder_ = LabelEncoder()
        y_ = self.target_encoder_.fit_transform(y)
        self.xgb_params_ = {
            "objective": "multi:softprob",
            "booster": "gblinear",
            "num_class": len(self.target_encoder_.classes_),
            "eval_metric": "mlogloss",
            'alpha': self.reg_alpha,
            'lambda': self.reg_lambda,
            'lambda_bias': self.reg_lambda_bias,
            'eta': self.learning_rate
        }
        X = self._append_ones_at_right(X)
        d = xgb.DMatrix(X, y_, missing=self.missing_)
        self.booster_ = xgb.train(self.xgb_params_, d, self.n_estimators)
        return self

    def predict_proba(self, X):
        X = self._append_ones_at_right(X)
        return self.booster_.predict(xgb.DMatrix(X, missing=self.missing_))

    def predict(self, X):
        prob = self.predict_proba(X)
        return self.target_encoder_.inverse_transform(prob.argmax(axis=1))
