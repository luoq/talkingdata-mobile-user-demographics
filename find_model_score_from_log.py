import re
from collections import OrderedDict
from config import ENABLED_MODELS
pattern = re.compile('^.+INFO (?P<model>.+?):.*log loss.*: (?P<score>.+?)$')
if __name__ == '__main__':
    import sys
    print('model,score')
    scores = OrderedDict()
    for line in sys.stdin:
        line = line.rstrip()
        res = pattern.match(line)
        if res is not None:
            scores[res.group('model')] = float(res.group('score'))
    for model, score in scores.items():
        if model in ENABLED_MODELS:
            print('{},{}'.format(model, score))