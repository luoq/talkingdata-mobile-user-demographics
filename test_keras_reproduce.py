import pickle

from keras.layers import Dense, Activation, Dropout
from keras.models import Sequential
from keras.optimizers import Nadam
from sklearn.feature_selection import VarianceThreshold
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import Binarizer

from util.keras import KerasClassifier
from feature.bag_of_app import *

groups = group_encoder.classes_
train_mask = gender_age.is_train.values
n_sample = gender_age.shape[0]
n_train = train_mask.sum()
n_test = len(train_mask) - n_train
y_train = gender_age.group.values[train_mask]
cv = StratifiedKFold(5, shuffle=True, random_state=0)

X = sp.hstack((phone_brand, phone_brand_model, installed_app_count, installed_app_label)).tocsr()

transformer = make_pipeline(VarianceThreshold(), Binarizer())
X = transformer.fit_transform(X)

X_train = X[train_mask]
X_test = X[~train_mask]

def gen_model(input_dim):
    model = Sequential([
        Dense(100, input_dim=input_dim, init='glorot_normal'),
        Dropout(0.5),
        Activation('relu'),
        Dense(30, init='glorot_normal'),
        Dropout(0.5),
        Activation('relu'),
        Dense(len(set(y_train)), init='glorot_normal'),
        Activation('softmax'),
    ])
    model.compile(
        optimizer=Nadam(lr=1e-4),
        loss='categorical_crossentropy',
        metrics=['accuracy'],
    )
    return model

def check_kereas_model(m1, m2):
    for a, b in zip(m1.get_weights(), m2.get_weights()):
        if not (a==b).all():
            return False
    return True

def check_model(m1, m2):
    for a, b in zip(m1.model_results_, m2.model_results_):
        if not check_kereas_model(a[2], b[2]):
            return False
    return True

model = KerasClassifier(gen_model, patience=1, repeat=5, verbose=1, max_iter=3, shuffle=True)
model.fit(X_train[:1000], y_train[:1000])

model2 = KerasClassifier(gen_model, patience=1, repeat=5, verbose=1, max_iter=3, shuffle=True)
model2.fit(X_train[:1000], y_train[:1000])

print(check_model(model, model2))