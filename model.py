import os

import numpy as np
import pandas as pd
from datatrek.util import with_cache
from keras.layers import Dense, Activation, Dropout
from keras.models import Sequential
from keras.optimizers import Nadam
from sklearn.dummy import DummyClassifier
from sklearn.ensemble import BaggingClassifier
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_selection import VarianceThreshold
from sklearn.linear_model import LogisticRegressionCV
from sklearn.metrics import log_loss
from sklearn.model_selection import cross_val_predict
from sklearn.neural_network import MLPClassifier
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import Binarizer, Imputer
from xgboost.sklearn import XGBClassifier

from config import config
from feature.bag_of_app import *
from feature.decomposition import *
from util import GBLinearClassifier
from util.keras import KerasClassifier

# raise recursion limit for pickle.dump
import sys

sys.setrecursionlimit(10000)

data_root = config['data_root']
data_subroot = 'model'
os.makedirs(os.path.join(data_root, data_subroot), exist_ok=True)

import logging

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(filename)s[line:%(lineno)d] %(levelname)s %(message)s',
                    filename=os.path.join('log', 'model.log'),
                    filemode='a')
logger = logging.getLogger('model')

# variables for data split
groups = group_encoder.classes_
train_mask = gender_age.is_train.values
n_sample = gender_age.shape[0]
n_train = train_mask.sum()
n_test = len(train_mask) - n_train
y_train = gender_age.group.values[train_mask]
cv = StratifiedKFold(5, shuffle=True, random_state=0)


# train model; target is default

def find_train_and_test(X, y_train, sample_filter, X_filtered=False):
    if sample_filter is not None:
        if X_filtered:
            n_train_local = sample_filter[train_mask].sum()
            X1 = X[:n_train_local]
            X2 = X[n_train_local:]
        else:
            X1 = X[train_mask & sample_filter]
            X2 = X[(~train_mask) & sample_filter]

        y1 = y_train[sample_filter[:n_train]]
    else:
        X1 = X[:n_train]
        X2 = X[n_train:]
        y1 = y_train
    return X1, X2, y1


def train_model(model, X, sample_filter=None, name=None, predict_cv_prob_on_train=True, X_filtered=False, n_jobs=5,
                shuffle=False, seed=42):
    X1, X2, y1 = find_train_and_test(X, y_train, sample_filter, X_filtered=X_filtered)

    if shuffle:
        rng = np.random.RandomState(seed)
        order = rng.permutation(len(y1))
        X1 = X1[order]
        y1 = y1[order]

    logger.info('{}: #active train samples: {}/{}'.format(name, X1.shape[0], n_train))
    logger.info('{}: #active test samples: {}/{}'.format(name, X2.shape[0], n_test))

    res = {}
    if predict_cv_prob_on_train:
        prob1 = cross_val_predict(model, X1, y1, cv=cv,
                                  method='predict_proba', n_jobs=n_jobs)
        res['prob_train_cv'] = prob1
        if sample_filter is not None:
            logger.info('{}: log loss(filtered) is: {:.4f}'.format(name, log_loss(y1, prob1)))
        else:
            logger.info('{}: log loss is: {:.4f}'.format(name, log_loss(y1, prob1)))
    model.fit(X1, y1)
    res['model'] = model
    res['prob_test'] = model.predict_proba(X2)
    if sample_filter is not None:
        res['sample_filter'] = sample_filter
    return res


test_set_original_device_id = device_encoder.inverse_transform(gender_age[~gender_age.is_train].device_id)
models = {}


def fit_to_nrow(X, nrow, mask):
    res = np.empty((nrow, X.shape[1])) * np.nan
    res[mask] = X
    return res


def get_model_prob(name, fill_to_full=True, concat=False):
    res = models[name]
    prob1 = res['prob_train_cv']
    prob2 = res['prob_test']
    if fill_to_full and 'sample_filter' in res:
        prob1 = fit_to_nrow(prob1, n_train, res['sample_filter'][:n_train])
        prob2 = fit_to_nrow(prob2, n_test, res['sample_filter'][n_train:])
    if concat:
        return np.vstack((prob1, prob2))
    else:
        return prob1, prob2


def get_model_probs_as_feature(names, mask=None):
    columns = []
    res = []
    for name in names:
        X = get_model_prob(name, fill_to_full=True, concat=True)
        if mask is not None:
            X = X[mask]
        res.append(X)
        columns.extend(['prob_{}_{}'.format(name, group) for group in groups])
    return pd.DataFrame(np.hstack(res), columns=columns)


def backoff(x, x_backoff, mask):
    y = x_backoff.copy()
    y[mask] = x
    return y


def average_probability(probs, weights):
    prob = (probs * np.array(weights).reshape((-1, 1, 1))).mean(axis=0)
    prob = prob / prob.sum(axis=1)[:, np.newaxis]
    return prob


def average_models(spec):
    names = []
    prob1s = []
    prob2s = []
    weights = []
    for name, weight in spec:
        names.append(name)
        weights.append(weight)
        prob1, prob2 = get_model_prob(name)
        prob1s.append(prob1)
        prob2s.append(prob2)
    prob1 = np.nan_to_num(np.array(prob1s))
    prob2 = np.nan_to_num(np.array(prob2s))
    prob1 = average_probability(prob1, weights)
    prob2 = average_probability(prob2, weights)
    return prob1, prob2


def to_submission(prob, sample_filter=None):
    if sample_filter is None:
        return pd.DataFrame(prob, index=test_set_original_device_id, columns=groups)
    else:
        return pd.DataFrame(prob, index=test_set_original_device_id[sample_filter[n_train:]], columns=groups)


def train_model_cached(name, model, gen_X, sample_filter=None, write_submision=True,
                       backoff_model_name=None, X_filtered=False, **kargs):
    res_file = os.path.join(data_root, data_subroot, name + '.pickle')

    def f():
        logger.info('{}: start training'.format(name))
        X = gen_X()
        res = train_model(model, X, sample_filter, name=name, X_filtered=X_filtered, **kargs)
        if write_submision:
            submission_file = os.path.join(data_root, data_subroot, name + '.submission.csv')
            prob2 = res['prob_test']
            if backoff_model_name is not None:
                prob2 = backoff(prob2, get_model_prob(backoff_model_name, concat=False)[1],
                                res['sample_filter'][n_train:])
                submission = to_submission(prob2, sample_filter=None)
            else:
                submission = to_submission(prob2, sample_filter)
            submission.to_csv(submission_file, index_label='device_id')
        logger.info('{}: finished training'.format(name))
        return res

    return with_cache(res_file)(f)()

from config import ENABLED_MODELS


def add_model(name, model, gen_X, sample_filter=None, **kargs):
    if name in ENABLED_MODELS:
        models[name] = train_model_cached(name, model, gen_X, sample_filter, **kargs)


# phone feature
add_model('lr_on_phone_brand_and_brand_model',
          LogisticRegressionCV(solver='liblinear', n_jobs=5,
                               scoring='log_loss'),
          lambda: sp.hstack((phone_brand, phone_brand_model)).tocsr())

add_model('lr_on_phone_brand_and_brand_model_for_device_without_installed_app_2',
          LogisticRegressionCV(solver='lbfgs', n_jobs=5,
                               scoring='log_loss'),
          lambda: sp.hstack((phone_brand, phone_brand_model)).tocsr(),
          sample_filter=~device_with_installed_app)

def gen_nn3(input_dim):
    model = Sequential([
        Dense(50, input_dim=input_dim, init='glorot_normal'),
        Dropout(0.3),
        Activation('relu'),
        Dense(len(groups), init='glorot_normal'),
        Activation('softmax'),
    ])
    model.compile(
        optimizer=Nadam(lr=1e-3),
        loss='categorical_crossentropy',
        metrics=['accuracy'],
    )
    return model

add_model('nn3_on_phone_for_device_with_installed_app',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        KerasClassifier(gen_nn3, patience=1, repeat=10, seed=46, verbose=1, method='average')),
          lambda: sp.hstack((phone_brand, phone_brand_model)).tocsr(),
          sample_filter=device_with_installed_app,
          backoff_model_name='lr_on_phone_brand_and_brand_model',
          n_jobs=1)

add_model('nn3_on_phone',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        KerasClassifier(gen_nn3, patience=1, repeat=10, seed=46, verbose=1, method='average')),
          lambda: sp.hstack((phone_brand, phone_brand_model)).tocsr(),
          n_jobs=1)

def gen_nn2(input_dim):
    model = Sequential([
        Dense(500, input_dim=input_dim, init='glorot_normal'),
        Dropout(0.5),
        Activation('relu'),
        Dense(len(groups), init='glorot_normal'),
        Activation('softmax'),
    ])
    model.compile(
        optimizer=Nadam(lr=1e-4),
        loss='categorical_crossentropy',
        metrics=['accuracy'],
    )
    return model

add_model('nn2_on_phone_and_installed_app_and_label_for_device_with_installed_app',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        KerasClassifier(gen_nn2, patience=1, repeat=10, seed=46, verbose=1, method='average')),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count, installed_app_label)).tocsr(),
          sample_filter=device_with_installed_app,
          backoff_model_name='lr_on_phone_brand_and_brand_model',
          n_jobs=1)

add_model('nn2_on_phone_and_installed_app_and_label',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        KerasClassifier(gen_nn2, patience=1, repeat=10, seed=47, verbose=1, method='average')),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count, installed_app_label)).tocsr(),
          n_jobs=1
          )

add_model('nn2_on_phone_and_installed_app_for_device_with_installed_app',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        KerasClassifier(gen_nn2, patience=1, repeat=10, seed=48, verbose=1, method='average')),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count)).tocsr(),
          sample_filter=device_with_installed_app,
          backoff_model_name='lr_on_phone_brand_and_brand_model',
          n_jobs=1)

add_model('nn2_on_phone_and_installed_app',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        KerasClassifier(gen_nn2, patience=1, repeat=10, seed=49, verbose=1, method='average')),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count)).tocsr(),
          n_jobs=1)

add_model('test',
          make_pipeline(VarianceThreshold(), Binarizer(), DummyClassifier(strategy='prior')),
          lambda: sp.hstack((phone_brand, phone_brand_model)).tocsr(),
          sample_filter=None)

# bag of app

add_model('lr_on_installed_app_ratio_for_device_with_installed_app',
          make_pipeline(VarianceThreshold(), LogisticRegressionCV(solver='lbfgs', n_jobs=5,
                                                                  scoring='log_loss')),
          lambda: installed_app_ratio,
          sample_filter=device_with_installed_app
          )

add_model('lr_on_active_app_ratio_for_device_with_installed_app',
          make_pipeline(VarianceThreshold(), LogisticRegressionCV(solver='lbfgs', n_jobs=5,
                                                                  scoring='log_loss')),
          lambda: active_app_ratio,
          sample_filter=device_with_installed_app
          )

add_model('lr_on_installed_and_active_app_ratio_for_device_with_installed_app',
          make_pipeline(VarianceThreshold(), LogisticRegressionCV(solver='lbfgs', n_jobs=5,
                                                                  scoring='log_loss')),
          lambda: sp.hstack((installed_app_ratio, active_app_ratio)).tocsr(),
          sample_filter=device_with_installed_app
          )

add_model('lr_on_installed_app_ratio_for_device_with_installed_app_2',
          make_pipeline(VarianceThreshold(), LogisticRegressionCV(solver='lbfgs', n_jobs=5,
                                                                  scoring='log_loss')),
          lambda: installed_app_ratio_2,
          sample_filter=device_with_installed_app
          )

add_model('lr_on_active_app_ratio_for_device_with_installed_app_2',
          make_pipeline(VarianceThreshold(), LogisticRegressionCV(solver='lbfgs', n_jobs=5,
                                                                  scoring='log_loss')),
          lambda: active_app_ratio_2,
          sample_filter=device_with_installed_app
          )

add_model('lr_on_installed_and_active_app_ratio_for_device_with_installed_app_2',
          make_pipeline(VarianceThreshold(), LogisticRegressionCV(solver='lbfgs', n_jobs=5,
                                                                  scoring='log_loss')),
          lambda: sp.hstack((installed_app_ratio_2, active_app_ratio_2)).tocsr(),
          sample_filter=device_with_installed_app
          )

add_model('lr_on_installed_and_active_app_for_device_with_installed_app',
          make_pipeline(VarianceThreshold(), Binarizer(), LogisticRegressionCV(solver='lbfgs', n_jobs=5,
                                                                               scoring='log_loss')),
          lambda: sp.hstack((installed_app_ratio_2, active_app_ratio_2)).tocsr(),
          sample_filter=device_with_installed_app
          )

add_model('lr_on_tfidf_installed_and_active_app_ratio_for_device_with_installed_app_2',
          make_pipeline(VarianceThreshold(), TfidfTransformer(), LogisticRegressionCV(solver='lbfgs', n_jobs=5,
                                                                                      scoring='log_loss')),
          lambda: sp.hstack((installed_app_ratio_2, active_app_ratio_2)).tocsr(),
          sample_filter=device_with_installed_app
          )

# bag of app label

add_model('lr_on_installed_app_label_for_device_with_installed_app',
          make_pipeline(VarianceThreshold(), LogisticRegressionCV(solver='lbfgs', n_jobs=5,
                                                                  scoring='log_loss')),
          lambda: installed_app_label,
          sample_filter=device_with_installed_app
          )

add_model('lr_on_active_app_label_for_device_with_installed_app',
          make_pipeline(VarianceThreshold(), LogisticRegressionCV(solver='lbfgs', n_jobs=5,
                                                                  scoring='log_loss')),
          lambda: active_app_label,
          sample_filter=device_with_installed_app
          )

add_model('lr_on_installed_and_active_app_label_for_device_with_installed_app',
          make_pipeline(VarianceThreshold(), LogisticRegressionCV(solver='lbfgs', n_jobs=5,
                                                                  scoring='log_loss')),
          lambda: sp.hstack((installed_app_label, active_app_label)).tocsr(),
          sample_filter=device_with_installed_app)

add_model('lr_on_binary_installed_and_active_app_label_for_device_with_installed_app',
          make_pipeline(Binarizer(), VarianceThreshold(), LogisticRegressionCV(solver='lbfgs', n_jobs=5,
                                                                               scoring='log_loss')),
          lambda: sp.hstack((installed_app_label, active_app_label)).tocsr(),
          sample_filter=device_with_installed_app)

add_model('lr_on_binary_tfidf_installed_and_active_app_label_for_device_with_installed_app',
          make_pipeline(VarianceThreshold(), Binarizer(), TfidfTransformer(),
                        LogisticRegressionCV(solver='lbfgs', n_jobs=5,
                                             scoring='log_loss')),
          lambda: sp.hstack((installed_app_label, active_app_label)).tocsr(),
          sample_filter=device_with_installed_app)

add_model('lr_on_installed_app_label_for_device_with_installed_app_2',
          make_pipeline(VarianceThreshold(), LogisticRegressionCV(solver='lbfgs', n_jobs=5,
                                                                  scoring='log_loss')),
          lambda: installed_app_label_2,
          sample_filter=device_with_installed_app
          )

add_model('lr_on_active_app_label_for_device_with_installed_app_2',
          make_pipeline(VarianceThreshold(), LogisticRegressionCV(solver='lbfgs', n_jobs=5,
                                                                  scoring='log_loss')),
          lambda: active_app_label_2,
          sample_filter=device_with_installed_app
          )

add_model('lr_on_installed_and_active_app_label_for_device_with_installed_app_2',
          make_pipeline(VarianceThreshold(), LogisticRegressionCV(solver='lbfgs', n_jobs=5,
                                                                  scoring='log_loss')),
          lambda: sp.hstack((installed_app_label_2, active_app_label_2)).tocsr(),
          sample_filter=device_with_installed_app)

add_model('lr_on_installed_app_for_device_with_installed_app',
          make_pipeline(VarianceThreshold(), Binarizer(), LogisticRegressionCV(solver='lbfgs', n_jobs=5,
                                                                               scoring='log_loss')),
          lambda: installed_app_count,
          sample_filter=device_with_installed_app)

add_model('mlp_on_installed_app_for_device_with_installed_app',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        MLPClassifier(hidden_layer_sizes=(500,), early_stopping=True, random_state=1)),
          lambda: installed_app_count,
          sample_filter=device_with_installed_app)

add_model('lr_on_installed_app_label_for_device_with_installed_app',
          make_pipeline(VarianceThreshold(), Binarizer(), LogisticRegressionCV(solver='lbfgs', n_jobs=5,
                                                                               scoring='log_loss')),
          lambda: installed_app_label_2,
          sample_filter=device_with_installed_app)

add_model('lr_on_installed_app_category_for_device_with_installed_app',
          make_pipeline(VarianceThreshold(), Binarizer(), LogisticRegressionCV(solver='lbfgs', n_jobs=5,
                                                                               scoring='log_loss')),
          lambda: installed_app_category_2,
          sample_filter=device_with_installed_app)

add_model('lr_on_active_app_for_device_with_installed_app',
          make_pipeline(VarianceThreshold(), Binarizer(), LogisticRegressionCV(solver='lbfgs', n_jobs=5,
                                                                               scoring='log_loss')),
          lambda: active_app_count,
          sample_filter=device_with_installed_app)

add_model('lr_on_active_app_label_for_device_with_installed_app',
          make_pipeline(VarianceThreshold(), Binarizer(), LogisticRegressionCV(solver='lbfgs', n_jobs=5,
                                                                               scoring='log_loss')),
          lambda: active_app_label_2,
          sample_filter=device_with_installed_app)

add_model('lr_on_active_app_category_for_device_with_installed_app',
          make_pipeline(VarianceThreshold(), Binarizer(), LogisticRegressionCV(solver='lbfgs', n_jobs=5,
                                                                               scoring='log_loss')),
          lambda: active_app_category_2,
          sample_filter=device_with_installed_app)

add_model('lr_on_phone_and_installed_app_for_device_with_installed_app',
          make_pipeline(VarianceThreshold(), Binarizer(), GBLinearClassifier(reg_alpha=3, reg_lambda=3)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count)).tocsr(),
          sample_filter=device_with_installed_app)

add_model('gblinear_on_phone_and_installed_app_for_device_with_installed_app',
          make_pipeline(VarianceThreshold(), Binarizer(), GBLinearClassifier(reg_alpha=3, reg_lambda=3)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count)).tocsr(),
          sample_filter=device_with_installed_app)

add_model('gblinear_on_phone_and_installed_app_for_device_with_installed_app_2',
          make_pipeline(VarianceThreshold(), Binarizer(), GBLinearClassifier(reg_alpha=3, reg_lambda=0)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count)).tocsr(),
          sample_filter=device_with_installed_app)

add_model('lr_on_phone_and_installed_app_and_label_for_device_with_installed_app',
          make_pipeline(VarianceThreshold(), Binarizer(), LogisticRegressionCV(solver='lbfgs', n_jobs=5,
                                                                               scoring='log_loss')),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count, installed_app_label)).tocsr(),
          sample_filter=device_with_installed_app,
          backoff_model_name='lr_on_phone_brand_and_brand_model')



def gen_nn1(input_dim):
    model = Sequential([
        Dense(500, input_dim=input_dim, init='glorot_normal'),
        Dropout(0.5),
        Activation('relu'),
        Dense(500, init='glorot_normal'),
        Dropout(0.5),
        Activation('relu'),
        Dense(len(groups), init='glorot_normal'),
        Activation('softmax'),
    ])
    model.compile(
        optimizer=Nadam(lr=1e-4),
        loss='categorical_crossentropy',
        metrics=['accuracy'],
    )
    return model


add_model('nn1_on_phone_and_installed_app_and_label_for_device_with_installed_app',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        KerasClassifier(gen_nn1, patience=1, repeat=1, seed=42, verbose=1)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count, installed_app_label)).tocsr(),
          sample_filter=device_with_installed_app,
          backoff_model_name='lr_on_phone_brand_and_brand_model',
          n_jobs=1)

add_model('nn1_on_phone_and_installed_app_and_label',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        KerasClassifier(gen_nn1, patience=1, repeat=1, seed=43, verbose=1)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count, installed_app_label)).tocsr(),
          n_jobs=1
          )

add_model('nn1_on_phone_and_installed_app_for_device_with_installed_app',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        KerasClassifier(gen_nn1, patience=1, repeat=1, seed=44, verbose=1)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count)).tocsr(),
          sample_filter=device_with_installed_app,
          backoff_model_name='lr_on_phone_brand_and_brand_model',
          n_jobs=1)

add_model('nn1_on_phone_and_installed_app',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        KerasClassifier(gen_nn1, patience=1, repeat=1, seed=45, verbose=1)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count)).tocsr(),
          n_jobs=1)

add_model('nn1_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        KerasClassifier(gen_nn1, patience=1, repeat=5, seed=46, verbose=1)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count, installed_app_label)).tocsr(),
          sample_filter=device_with_installed_app,
          backoff_model_name='lr_on_phone_brand_and_brand_model',
          n_jobs=1)

add_model('nn1_on_phone_and_installed_app_and_label_2',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        KerasClassifier(gen_nn1, patience=1, repeat=5, seed=47, verbose=1)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count, installed_app_label)).tocsr(),
          n_jobs=1
          )

add_model('nn1_on_phone_and_installed_app_for_device_with_installed_app_2',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        KerasClassifier(gen_nn1, patience=1, repeat=5, seed=48, verbose=1)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count)).tocsr(),
          sample_filter=device_with_installed_app,
          backoff_model_name='lr_on_phone_brand_and_brand_model',
          n_jobs=1)

add_model('nn1_on_phone_and_installed_app_2',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        KerasClassifier(gen_nn1, patience=1, repeat=5, seed=49, verbose=1)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count)).tocsr(),
          n_jobs=1)

add_model('nn1_on_phone_and_installed_app_and_label_for_device_with_installed_app_3',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        KerasClassifier(gen_nn1, patience=1, repeat=10, seed=46, verbose=1)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count, installed_app_label)).tocsr(),
          sample_filter=device_with_installed_app,
          backoff_model_name='lr_on_phone_brand_and_brand_model',
          n_jobs=1)

add_model('nn1_on_phone_and_installed_app_and_label_3',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        KerasClassifier(gen_nn1, patience=1, repeat=10, seed=47, verbose=1)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count, installed_app_label)).tocsr(),
          n_jobs=1
          )

add_model('nn1_on_phone_and_installed_app_for_device_with_installed_app_3',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        KerasClassifier(gen_nn1, patience=1, repeat=10, seed=48, verbose=1)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count)).tocsr(),
          sample_filter=device_with_installed_app,
          backoff_model_name='lr_on_phone_brand_and_brand_model',
          n_jobs=1)

add_model('nn1_on_phone_and_installed_app_3',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        KerasClassifier(gen_nn1, patience=1, repeat=10, seed=49, verbose=1)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count)).tocsr(),
          n_jobs=1)

add_model('nn1_on_installed_app_and_label_for_device_with_installed_app_3',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        KerasClassifier(gen_nn1, patience=1, repeat=10, seed=48, verbose=1)),
          lambda: sp.hstack((installed_app_count, installed_app_label)).tocsr(),
          sample_filter=device_with_installed_app,
          backoff_model_name='lr_on_phone_brand_and_brand_model',
          n_jobs=1)

add_model('nn1_on_installed_app_for_device_with_installed_app_3',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        KerasClassifier(gen_nn1, patience=1, repeat=10, seed=48, verbose=1)),
          lambda: installed_app_count,
          sample_filter=device_with_installed_app,
          backoff_model_name='lr_on_phone_brand_and_brand_model',
          n_jobs=1)

add_model('nn1_on_installed_app_label_for_device_with_installed_app_3',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        KerasClassifier(gen_nn1, patience=1, repeat=10, seed=48, verbose=1)),
          lambda: installed_app_label,
          sample_filter=device_with_installed_app,
          backoff_model_name='lr_on_phone_brand_and_brand_model',
          n_jobs=1)

add_model('nn1_on_installed_app_category_for_device_with_installed_app_3',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        KerasClassifier(gen_nn1, patience=1, repeat=10, seed=48, verbose=1)),
          lambda: installed_app_category_2,
          sample_filter=device_with_installed_app,
          backoff_model_name='lr_on_phone_brand_and_brand_model',
          n_jobs=1)

add_model('nn1_on_installed_app_ratio_for_device_with_installed_app_3',
          make_pipeline(VarianceThreshold(),
                        KerasClassifier(gen_nn1, patience=1, repeat=10, seed=48, verbose=1)),
          lambda: installed_app_ratio_2,
          sample_filter=device_with_installed_app,
          backoff_model_name='lr_on_phone_brand_and_brand_model',
          n_jobs=1)


add_model('nn1_on_phone_and_installed_app_and_label_for_device_with_installed_app_4',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        KerasClassifier(gen_nn1, patience=1, repeat=10, seed=46, verbose=1, method='best')),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count, installed_app_label)).tocsr(),
          sample_filter=device_with_installed_app,
          backoff_model_name='lr_on_phone_brand_and_brand_model',
          n_jobs=1)

add_model('nn1_on_phone_and_installed_app_and_label_4',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        KerasClassifier(gen_nn1, patience=1, repeat=10, seed=47, verbose=1, method='best')),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count, installed_app_label)).tocsr(),
          n_jobs=1
          )

add_model('nn1_on_phone_and_installed_app_for_device_with_installed_app_4',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        KerasClassifier(gen_nn1, patience=1, repeat=10, seed=48, verbose=1, method='best')),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count)).tocsr(),
          sample_filter=device_with_installed_app,
          backoff_model_name='lr_on_phone_brand_and_brand_model',
          n_jobs=1)

add_model('nn1_on_phone_and_installed_app_4',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        KerasClassifier(gen_nn1, patience=1, repeat=10, seed=49, verbose=1, method='best')),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count)).tocsr(),
          n_jobs=1)


add_model('mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        MLPClassifier(hidden_layer_sizes=(500,), early_stopping=True, random_state=0)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count, installed_app_label)).tocsr(),
          sample_filter=device_with_installed_app,
          backoff_model_name='lr_on_phone_brand_and_brand_model')

add_model('mlp_on_phone_and_installed_app_and_label',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        MLPClassifier(hidden_layer_sizes=(500,), early_stopping=True, random_state=1)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count, installed_app_label)).tocsr(),
          )

add_model('mlp_on_phone_and_installed_app_for_device_with_installed_app',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        MLPClassifier(hidden_layer_sizes=(500,), early_stopping=True, random_state=2)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count)).tocsr(),
          sample_filter=device_with_installed_app,
          backoff_model_name='lr_on_phone_brand_and_brand_model')

add_model('mlp_on_phone_and_installed_app',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        MLPClassifier(hidden_layer_sizes=(500,), early_stopping=True, random_state=3)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count)).tocsr(),
          )

add_model('mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        MLPClassifier(hidden_layer_sizes=(500,), early_stopping=True, random_state=4)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count, installed_app_label)).tocsr(),
          sample_filter=device_with_installed_app,
          backoff_model_name='lr_on_phone_brand_and_brand_model')

add_model('mlp_on_phone_and_installed_app_and_label_2',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        MLPClassifier(hidden_layer_sizes=(500,), early_stopping=True, random_state=5)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count, installed_app_label)).tocsr(),
          )

add_model('mlp_on_phone_and_installed_app_for_device_with_installed_app_2',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        MLPClassifier(hidden_layer_sizes=(500,), early_stopping=True, random_state=6)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count)).tocsr(),
          sample_filter=device_with_installed_app,
          backoff_model_name='lr_on_phone_brand_and_brand_model')

add_model('mlp_on_phone_and_installed_app_2',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        MLPClassifier(hidden_layer_sizes=(500,), early_stopping=True, random_state=7)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count)).tocsr(),
          )

add_model('mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app_3',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        MLPClassifier(hidden_layer_sizes=(500,), early_stopping=True, alpha=1e-3, random_state=0)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count, installed_app_label)).tocsr(),
          sample_filter=device_with_installed_app,
          backoff_model_name='lr_on_phone_brand_and_brand_model')

add_model('mlp_on_phone_and_installed_app_and_label_3',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        MLPClassifier(hidden_layer_sizes=(500,), early_stopping=True, alpha=1e-3, random_state=1)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count, installed_app_label)).tocsr(),
          )

add_model('mlp_on_phone_and_installed_app_for_device_with_installed_app_3',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        MLPClassifier(hidden_layer_sizes=(500,), early_stopping=True, alpha=1e-3, random_state=2)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count)).tocsr(),
          sample_filter=device_with_installed_app,
          backoff_model_name='lr_on_phone_brand_and_brand_model')

add_model('mlp_on_phone_and_installed_app_3',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        MLPClassifier(hidden_layer_sizes=(500,), early_stopping=True, alpha=1e-3, random_state=3)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count)).tocsr(),
          )

add_model('mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app_4',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        MLPClassifier(hidden_layer_sizes=(200,), early_stopping=True, alpha=1e-4, random_state=0)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count, installed_app_label)).tocsr(),
          sample_filter=device_with_installed_app,
          backoff_model_name='lr_on_phone_brand_and_brand_model')

add_model('mlp_on_phone_and_installed_app_and_label_4',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        MLPClassifier(hidden_layer_sizes=(200,), early_stopping=True, alpha=1e-4, random_state=1)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count, installed_app_label)).tocsr(),
          )

add_model('mlp_on_phone_and_installed_app_for_device_with_installed_app_4',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        MLPClassifier(hidden_layer_sizes=(200,), early_stopping=True, alpha=1e-4, random_state=2)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count)).tocsr(),
          sample_filter=device_with_installed_app,
          backoff_model_name='lr_on_phone_brand_and_brand_model')

add_model('mlp_on_phone_and_installed_app_4',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        MLPClassifier(hidden_layer_sizes=(200,), early_stopping=True, alpha=1e-4, random_state=3)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count)).tocsr(),
          )

add_model('mlp_on_phone_and_installed_app_5',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        MLPClassifier(hidden_layer_sizes=(500,), early_stopping=True, alpha=1e-4, random_state=3)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count)).tocsr(),
          shuffle=True
          )

add_model('mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app_6',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        MLPClassifier(hidden_layer_sizes=(1000,), early_stopping=True, alpha=1e-4, random_state=0)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count, installed_app_label)).tocsr(),
          sample_filter=device_with_installed_app,
          backoff_model_name='lr_on_phone_brand_and_brand_model')

add_model('mlp_on_phone_and_installed_app_and_label_6',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        MLPClassifier(hidden_layer_sizes=(1000,), early_stopping=True, alpha=1e-4, random_state=1)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count, installed_app_label)).tocsr(),
          )

add_model('mlp_on_phone_and_installed_app_for_device_with_installed_app_6',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        MLPClassifier(hidden_layer_sizes=(1000,), early_stopping=True, alpha=1e-4, random_state=2)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count)).tocsr(),
          sample_filter=device_with_installed_app,
          backoff_model_name='lr_on_phone_brand_and_brand_model')

add_model('mlp_on_phone_and_installed_app_6',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        MLPClassifier(hidden_layer_sizes=(1000,), early_stopping=True, alpha=1e-4, random_state=3)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count)).tocsr(),
          )

add_model('mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app_7',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        MLPClassifier(hidden_layer_sizes=(1000,), early_stopping=True, alpha=1e-4,
                                      learning_rate_init=1e-4, batch_size=32, random_state=0)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count, installed_app_label)).tocsr(),
          sample_filter=device_with_installed_app,
          backoff_model_name='lr_on_phone_brand_and_brand_model')

add_model('mlp_on_phone_and_installed_app_and_label_7',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        MLPClassifier(hidden_layer_sizes=(1000,), early_stopping=True, alpha=1e-4,
                                      learning_rate_init=1e-4, batch_size=32, random_state=1)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count, installed_app_label)).tocsr(),
          )

add_model('mlp_on_phone_and_installed_app_for_device_with_installed_app_7',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        MLPClassifier(hidden_layer_sizes=(1000,), early_stopping=True, alpha=1e-4,
                                      learning_rate_init=1e-4, batch_size=32, random_state=2)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count)).tocsr(),
          sample_filter=device_with_installed_app,
          backoff_model_name='lr_on_phone_brand_and_brand_model')

add_model('mlp_on_phone_and_installed_app_7',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        MLPClassifier(hidden_layer_sizes=(1000,), early_stopping=True, alpha=1e-4,
                                      learning_rate_init=1e-4, batch_size=32, random_state=3)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count)).tocsr(),
          )

add_model('bagging_mlp_on_phone_and_installed_app',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        BaggingClassifier(MLPClassifier(hidden_layer_sizes=(500,), early_stopping=True, random_state=3),
                                          n_estimators=10, n_jobs=-1)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count)).tocsr(),
          )

add_model('gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        GBLinearClassifier(reg_alpha=3, reg_lambda=3)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count, installed_app_label)).tocsr(),
          sample_filter=device_with_installed_app,
          backoff_model_name='lr_on_phone_brand_and_brand_model')

add_model('gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
          make_pipeline(VarianceThreshold(), Binarizer(),
                        GBLinearClassifier(reg_alpha=3, reg_lambda=0)),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count, installed_app_label)).tocsr(),
          sample_filter=device_with_installed_app)

add_model('lr_on_phone_and_active_app_and_label_for_device_with_installed_app',
          make_pipeline(VarianceThreshold(), Binarizer(), LogisticRegressionCV(solver='lbfgs', n_jobs=5,
                                                                               scoring='log_loss')),
          lambda: sp.hstack((phone_brand, phone_brand_model, active_app_count, active_app_label)).tocsr(),
          sample_filter=device_with_installed_app)

add_model('lr_on_phone_and_installed_app_and_label_and_category_for_device_with_installed_app',
          make_pipeline(VarianceThreshold(), Binarizer(), LogisticRegressionCV(solver='lbfgs', n_jobs=5,
                                                                               scoring='log_loss')),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count, installed_app_label,
                             installed_app_category_2)).tocsr(),
          sample_filter=device_with_installed_app)

add_model('lr_on_phone_and_installed_and_active_app_for_device_with_installed_app',
          make_pipeline(VarianceThreshold(), Binarizer(), LogisticRegressionCV(solver='lbfgs', n_jobs=5,
                                                                               scoring='log_loss')),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count, active_app_count)).tocsr(),
          sample_filter=device_with_installed_app)

add_model('lr_on_phone_and_installed_app_for_device',
          make_pipeline(VarianceThreshold(), Binarizer(), LogisticRegressionCV(solver='lbfgs', n_jobs=5,
                                                                               scoring='log_loss')),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count)).tocsr(),
          sample_filter=None)

add_model('lr_on_phone_and_installed_app_by_hour_for_device_with_installed_app',
          make_pipeline(VarianceThreshold(), Binarizer(), LogisticRegressionCV(solver='lbfgs', n_jobs=5,
                                                                               scoring='log_loss')),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count_by_hour)).tocsr(),
          sample_filter=device_with_installed_app)

add_model('lr_on_phone_and_active_app_by_hour_for_device_with_installed_app',
          make_pipeline(VarianceThreshold(), Binarizer(), LogisticRegressionCV(solver='lbfgs', n_jobs=5,
                                                                               scoring='log_loss')),
          lambda: sp.hstack((phone_brand, phone_brand_model, active_app_count_by_hour)).tocsr(),
          sample_filter=device_with_installed_app)

# add_model('lr_on_phone_and_installed_and_active_app_by_hour_for_device_with_installed_app',
#           make_pipeline(VarianceThreshold(), Binarizer(), LogisticRegressionCV(solver='lbfgs', n_jobs=5,
#                                                                                scoring='log_loss')),
#           lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count_by_hour, active_app_count_by_hour)).tocsr(),
#           sample_filter=device_with_installed_app)

add_model('lr_on_phone_and_installed_app_by_hour_for_device_with_installed_app_2',
          make_pipeline(VarianceThreshold(), Binarizer(), LogisticRegressionCV(solver='sag', n_jobs=5,
                                                                               scoring='log_loss')),
          lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count_by_hour)).tocsr(),
          sample_filter=device_with_installed_app)

add_model('lr_on_phone_and_active_app_by_hour_for_device_with_installed_app_2',
          make_pipeline(VarianceThreshold(), Binarizer(), LogisticRegressionCV(solver='sag', n_jobs=5,
                                                                               scoring='log_loss')),
          lambda: sp.hstack((phone_brand, phone_brand_model, active_app_count_by_hour)).tocsr(),
          sample_filter=device_with_installed_app)

# add_model('lr_on_phone_and_installed_and_active_app_by_hour_for_device_with_installed_app_2',
#           make_pipeline(VarianceThreshold(), Binarizer(), LogisticRegressionCV(solver='sag', n_jobs=5,
#                                                                                scoring='log_loss')),
#           lambda: sp.hstack((phone_brand, phone_brand_model, installed_app_count_by_hour, active_app_count_by_hour)).tocsr(),
#           sample_filter=device_with_installed_app)

# add_model('mnb_on_installed_app_count_for_device_with_installed_app',
#           make_pipeline(VarianceThreshold(), Normalizer(norm='l1'),
#                         GridSearchCV(MultinomialNB(), {'alpha': [0.01, 0.1, 1, 10, 100]}, scoring='log_loss', n_jobs=5)),
#           lambda : installed_app_count,
#           sample_filter=device_with_installed_app)

# add_model('mnb_on_installed_app_for_device_with_installed_app',
#           make_pipeline(VarianceThreshold(), Binarizer(),
#                         GridSearchCV(MultinomialNB(), {'alpha': [0.01, 0.1, 1, 10, 100]}, scoring='log_loss', n_jobs=5)),
#           lambda : installed_app_count,
#           sample_filter=device_with_installed_app)

from feature.statistics import feats_df, get_full_feats_df

add_model('gbtree_feats',
          XGBClassifier(learning_rate=0.1, max_depth=3, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=70,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: feats_df[['n_event', 'n_event_with_no_app', 'ratio_event_with_no_app',
                            'n_app_installed_in_any_event', 'n_active_in_any_event',
                            'mean_installed_app_count_in_event', 'mean_active_app_count_in_event',
                            'mean_ratio_of_active_app_in_event', 'n_event_with_no_active_app',
                            'ratio_event_with_no_active_app', 'mean_app_ratio_of_installed',
                            'std_app_ratio_of_installed', 'mean_longitude', 'mean_latitude',
                            'var_longitude', 'var_latitude',
                            'ratio_of_event_without_valid_location']],
          device_with_event, X_filtered=True)

add_model('lr_on_installed_app_svd_200_for_device_with_installed_app',
          LogisticRegressionCV(solver='lbfgs', n_jobs=5,
                               scoring='log_loss'),
          lambda: installed_app_svd_200,
          sample_filter=device_with_installed_app, X_filtered=True,
          backoff_model_name='lr_on_phone_brand_and_brand_model')

add_model('lr_on_installed_app_nmf_200_for_device_with_installed_app',
          LogisticRegressionCV(solver='lbfgs', n_jobs=5,
                               scoring='log_loss'),
          lambda: installed_app_nmf_200,
          sample_filter=device_with_installed_app, X_filtered=True,
          backoff_model_name='lr_on_phone_brand_and_brand_model')

add_model('lr_on_installed_app_svd_500_for_device_with_installed_app',
          LogisticRegressionCV(solver='lbfgs', n_jobs=5,
                               scoring='log_loss'),
          lambda: installed_app_svd_500,
          sample_filter=device_with_installed_app, X_filtered=True,
          backoff_model_name='lr_on_phone_brand_and_brand_model')

add_model('lr_on_installed_app_nmf_500_for_device_with_installed_app',
          LogisticRegressionCV(solver='lbfgs', n_jobs=5,
                               scoring='log_loss'),
          lambda: installed_app_nmf_500,
          sample_filter=device_with_installed_app, X_filtered=True,
          backoff_model_name='lr_on_phone_brand_and_brand_model')

## statistics

## Level 2 models



add_model('stacking_1',
          XGBClassifier(learning_rate=0.1, max_depth=3, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=70,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: pd.concat((feats_df[['n_event', 'n_event_with_no_app', 'ratio_event_with_no_app',
                                       'n_app_installed_in_any_event', 'n_active_in_any_event',
                                       'mean_installed_app_count_in_event',
                                       'mean_active_app_count_in_event',
                                       'mean_ratio_of_active_app_in_event',
                                       'n_event_with_no_active_app',
                                       'ratio_event_with_no_active_app',
                                       'mean_app_ratio_of_installed',
                                       'std_app_ratio_of_installed']],
                             get_model_probs_as_feature([
                                 'lr_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'lr_on_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_phone_and_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_installed_app_ratio_for_device_with_installed_app_2',
                                 'lr_on_active_app_for_device_with_installed_app'
                             ], mask=device_with_event)), axis=1, ignore_index=True),
          device_with_event, X_filtered=True,
          backoff_model_name='lr_on_phone_brand_and_brand_model')

add_model('stacking_2',
          XGBClassifier(learning_rate=0.1, max_depth=3, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=70,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: pd.concat((feats_df[['n_event', 'n_event_with_no_app', 'ratio_event_with_no_app',
                                       'n_app_installed_in_any_event', 'n_active_in_any_event',
                                       'mean_installed_app_count_in_event',
                                       'mean_active_app_count_in_event',
                                       'mean_ratio_of_active_app_in_event',
                                       'n_event_with_no_active_app',
                                       'ratio_event_with_no_active_app',
                                       'mean_app_ratio_of_installed',
                                       'std_app_ratio_of_installed', 'mean_longitude',
                                       'mean_latitude']],
                             get_model_probs_as_feature([
                                 'lr_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'lr_on_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_phone_and_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_installed_app_ratio_for_device_with_installed_app_2',
                                 'lr_on_active_app_for_device_with_installed_app'
                             ], mask=device_with_event),), axis=1, ignore_index=True),
          device_with_event, X_filtered=True,
          backoff_model_name='lr_on_phone_brand_and_brand_model')

add_model('stacking_3',
          XGBClassifier(learning_rate=0.1, max_depth=3, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=70,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: pd.concat((feats_df[['n_event', 'n_event_with_no_app', 'ratio_event_with_no_app',
                                       'n_app_installed_in_any_event', 'n_active_in_any_event',
                                       'mean_installed_app_count_in_event',
                                       'mean_active_app_count_in_event',
                                       'mean_ratio_of_active_app_in_event',
                                       'n_event_with_no_active_app',
                                       'ratio_event_with_no_active_app',
                                       'mean_app_ratio_of_installed',
                                       'std_app_ratio_of_installed', 'mean_longitude',
                                       'mean_latitude']],
                             get_model_probs_as_feature([
                                 'lr_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'lr_on_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_installed_app_for_device_with_installed_app',
                                 'lr_on_phone_and_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_installed_app_ratio_for_device_with_installed_app_2',
                                 'lr_on_active_app_for_device_with_installed_app',
                                 'lr_on_phone_and_active_app_by_hour_for_device_with_installed_app_2',
                                 'lr_on_phone_and_installed_app_by_hour_for_device_with_installed_app_2',
                             ], mask=device_with_event),), axis=1, ignore_index=True),
          device_with_event, X_filtered=True,
          backoff_model_name='lr_on_phone_brand_and_brand_model')

add_model('stacking_4',
          XGBClassifier(learning_rate=0.1, max_depth=3, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=70,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: pd.concat((feats_df[['n_event', 'n_event_with_no_app', 'ratio_event_with_no_app',
                                       'n_app_installed_in_any_event', 'n_active_in_any_event',
                                       'mean_installed_app_count_in_event',
                                       'mean_active_app_count_in_event',
                                       'mean_ratio_of_active_app_in_event',
                                       'n_event_with_no_active_app',
                                       'ratio_event_with_no_active_app',
                                       'mean_app_ratio_of_installed',
                                       'std_app_ratio_of_installed', 'mean_longitude',
                                       'mean_latitude', 'var_longitude',
                                       'var_latitude']],
                             get_model_probs_as_feature([
                                 'lr_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'lr_on_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_phone_and_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_installed_app_ratio_for_device_with_installed_app_2',
                                 'lr_on_active_app_for_device_with_installed_app'
                             ], mask=device_with_event),), axis=1, ignore_index=True),
          device_with_event, X_filtered=True,
          backoff_model_name='lr_on_phone_brand_and_brand_model')

add_model('stacking_5',
          XGBClassifier(learning_rate=0.1, max_depth=3, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=70,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: pd.concat((feats_df[['n_event', 'n_event_with_no_app', 'ratio_event_with_no_app',
                                       'n_app_installed_in_any_event', 'n_active_in_any_event',
                                       'mean_installed_app_count_in_event',
                                       'mean_active_app_count_in_event',
                                       'mean_ratio_of_active_app_in_event',
                                       'n_event_with_no_active_app',
                                       'ratio_event_with_no_active_app',
                                       'mean_app_ratio_of_installed',
                                       'std_app_ratio_of_installed', 'mean_longitude',
                                       'mean_latitude', 'var_longitude',
                                       'var_latitude']],
                             get_model_probs_as_feature([
                                 'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'lr_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'lr_on_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_phone_and_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_installed_app_ratio_for_device_with_installed_app_2',
                                 'lr_on_active_app_for_device_with_installed_app'
                             ], mask=device_with_event),), axis=1, ignore_index=True),
          device_with_event, X_filtered=True,
          backoff_model_name='lr_on_phone_brand_and_brand_model')

add_model('stacking_6',
          XGBClassifier(learning_rate=0.1, max_depth=3, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=70,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: pd.concat((feats_df[['n_event', 'n_event_with_no_app', 'ratio_event_with_no_app',
                                       'n_app_installed_in_any_event', 'n_active_in_any_event',
                                       'mean_installed_app_count_in_event',
                                       'mean_active_app_count_in_event',
                                       'mean_ratio_of_active_app_in_event',
                                       'n_event_with_no_active_app',
                                       'ratio_event_with_no_active_app',
                                       'mean_app_ratio_of_installed',
                                       'std_app_ratio_of_installed', 'mean_longitude',
                                       'mean_latitude', 'var_longitude',
                                       'var_latitude']],
                             get_model_probs_as_feature([
                                 'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
                                 'lr_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'lr_on_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_phone_and_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_installed_app_ratio_for_device_with_installed_app_2',
                                 'lr_on_active_app_for_device_with_installed_app'
                             ], mask=device_with_event),), axis=1, ignore_index=True),
          device_with_event, X_filtered=True,
          backoff_model_name='lr_on_phone_brand_and_brand_model')

add_model('stacking_7',
          XGBClassifier(learning_rate=0.1, max_depth=3, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=70,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: pd.concat((feats_df[['n_event', 'n_event_with_no_app', 'ratio_event_with_no_app',
                                       'n_app_installed_in_any_event', 'n_active_in_any_event',
                                       'mean_installed_app_count_in_event',
                                       'mean_active_app_count_in_event',
                                       'mean_ratio_of_active_app_in_event',
                                       'n_event_with_no_active_app',
                                       'ratio_event_with_no_active_app',
                                       'mean_app_ratio_of_installed',
                                       'std_app_ratio_of_installed', 'mean_longitude',
                                       'mean_latitude', 'var_longitude',
                                       'var_latitude']],
                             get_model_probs_as_feature([
                                 'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
                                 'gblinear_on_phone_and_installed_app_for_device_with_installed_app',
                                 'gblinear_on_phone_and_installed_app_for_device_with_installed_app_2',
                                 'lr_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'lr_on_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_phone_and_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_installed_app_ratio_for_device_with_installed_app_2',
                                 'lr_on_active_app_for_device_with_installed_app'
                             ], mask=device_with_event),), axis=1, ignore_index=True),
          device_with_event, X_filtered=True,
          backoff_model_name='lr_on_phone_brand_and_brand_model')

add_model('stacking_8',
          XGBClassifier(learning_rate=0.1, max_depth=3, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=70,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: pd.concat((feats_df[['n_event', 'n_event_with_no_app', 'ratio_event_with_no_app',
                                       'n_app_installed_in_any_event', 'n_active_in_any_event',
                                       'mean_installed_app_count_in_event',
                                       'mean_active_app_count_in_event',
                                       'mean_ratio_of_active_app_in_event',
                                       'n_event_with_no_active_app',
                                       'ratio_event_with_no_active_app',
                                       'mean_app_ratio_of_installed',
                                       'std_app_ratio_of_installed', 'mean_longitude',
                                       'mean_latitude', 'var_longitude',
                                       'var_latitude']],
                             get_model_probs_as_feature([
                                 'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'gblinear_on_phone_and_installed_app_for_device_with_installed_app',
                                 'lr_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'lr_on_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_phone_and_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_installed_app_ratio_for_device_with_installed_app_2',
                                 'lr_on_active_app_for_device_with_installed_app'
                             ], mask=device_with_event),), axis=1, ignore_index=True),
          device_with_event, X_filtered=True,
          backoff_model_name='lr_on_phone_brand_and_brand_model')

add_model('stacking_9',
          XGBClassifier(learning_rate=0.1, max_depth=3, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=70,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: pd.concat((feats_df[['n_event', 'n_event_with_no_app', 'ratio_event_with_no_app',
                                       'n_app_installed_in_any_event', 'n_active_in_any_event',
                                       'mean_installed_app_count_in_event', 'mean_active_app_count_in_event',
                                       'mean_ratio_of_active_app_in_event', 'n_event_with_no_active_app',
                                       'ratio_event_with_no_active_app', 'mean_app_ratio_of_installed',
                                       'std_app_ratio_of_installed', 'mean_longitude', 'mean_latitude',
                                       'var_longitude', 'var_latitude',
                                       'ratio_of_event_without_valid_location']],
                             get_model_probs_as_feature([
                                 'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
                                 'gblinear_on_phone_and_installed_app_for_device_with_installed_app',
                                 'gblinear_on_phone_and_installed_app_for_device_with_installed_app_2',
                                 'lr_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'lr_on_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_phone_and_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_installed_app_ratio_for_device_with_installed_app_2',
                                 'lr_on_active_app_for_device_with_installed_app'
                             ], mask=device_with_event),), axis=1, ignore_index=True),
          device_with_event, X_filtered=True,
          backoff_model_name='lr_on_phone_brand_and_brand_model')

add_model('stacking_10',
          XGBClassifier(learning_rate=0.1, max_depth=3, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=70,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: pd.concat((feats_df[['n_event', 'n_event_with_no_app', 'ratio_event_with_no_app',
                                       'n_app_installed_in_any_event', 'n_active_in_any_event',
                                       'mean_installed_app_count_in_event', 'mean_active_app_count_in_event',
                                       'mean_ratio_of_active_app_in_event', 'n_event_with_no_active_app',
                                       'ratio_event_with_no_active_app', 'mean_app_ratio_of_installed',
                                       'std_app_ratio_of_installed', 'mean_longitude', 'mean_latitude',
                                       'var_longitude', 'var_latitude',
                                       'ratio_of_event_without_valid_location']],
                             get_model_probs_as_feature([
                                 'gbtree_feats',
                                 'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
                                 'gblinear_on_phone_and_installed_app_for_device_with_installed_app',
                                 'gblinear_on_phone_and_installed_app_for_device_with_installed_app_2',
                                 'lr_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'lr_on_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_phone_and_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_installed_app_ratio_for_device_with_installed_app_2',
                                 'lr_on_active_app_for_device_with_installed_app'
                             ], mask=device_with_event),), axis=1, ignore_index=True),
          device_with_event, X_filtered=True,
          backoff_model_name='lr_on_phone_brand_and_brand_model')

add_model('stacking_11',
          XGBClassifier(learning_rate=0.1, max_depth=3, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=70,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: get_model_probs_as_feature([
              'gbtree_feats',
              'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app',
              'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
              'gblinear_on_phone_and_installed_app_for_device_with_installed_app',
              'gblinear_on_phone_and_installed_app_for_device_with_installed_app_2',
              'lr_on_phone_and_installed_app_and_label_for_device_with_installed_app',
              'lr_on_installed_and_active_app_for_device_with_installed_app',
              'lr_on_phone_and_installed_and_active_app_for_device_with_installed_app',
              'lr_on_installed_app_ratio_for_device_with_installed_app_2',
              'lr_on_active_app_for_device_with_installed_app'
          ], mask=device_with_event),
          device_with_event, X_filtered=True,
          backoff_model_name='lr_on_phone_brand_and_brand_model')

add_model('stacking_12',
          XGBClassifier(learning_rate=0.1, max_depth=3, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=70,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: get_model_probs_as_feature([
              'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app',
              'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
              'gblinear_on_phone_and_installed_app_for_device_with_installed_app',
              'gblinear_on_phone_and_installed_app_for_device_with_installed_app_2',
              'lr_on_phone_and_installed_app_and_label_for_device_with_installed_app',
              'lr_on_installed_and_active_app_for_device_with_installed_app',
              'lr_on_phone_and_installed_and_active_app_for_device_with_installed_app',
              'lr_on_installed_app_ratio_for_device_with_installed_app_2',
              'lr_on_active_app_for_device_with_installed_app'
          ], mask=device_with_event),
          device_with_event, X_filtered=True,
          backoff_model_name='lr_on_phone_brand_and_brand_model')

add_model('stacking_13',
          XGBClassifier(learning_rate=0.1, max_depth=3, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=70,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: get_model_probs_as_feature([
              'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app',
              'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app',
              'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
              'gblinear_on_phone_and_installed_app_for_device_with_installed_app',
              'gblinear_on_phone_and_installed_app_for_device_with_installed_app_2',
              'lr_on_phone_and_installed_app_and_label_for_device_with_installed_app',
              'lr_on_installed_and_active_app_for_device_with_installed_app',
              'lr_on_phone_and_installed_and_active_app_for_device_with_installed_app',
              'lr_on_installed_app_ratio_for_device_with_installed_app_2',
              'lr_on_active_app_for_device_with_installed_app'
          ], mask=device_with_event),
          device_with_event, X_filtered=True,
          backoff_model_name='lr_on_phone_brand_and_brand_model')

add_model('stacking_14',
          XGBClassifier(learning_rate=0.1, max_depth=3, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=70,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: get_model_probs_as_feature([
              'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app',
              'lr_on_installed_and_active_app_for_device_with_installed_app',
              'lr_on_phone_and_installed_and_active_app_for_device_with_installed_app',
              'lr_on_installed_app_ratio_for_device_with_installed_app_2',
              'lr_on_active_app_for_device_with_installed_app'
          ], mask=device_with_event),
          device_with_event, X_filtered=True,
          backoff_model_name='lr_on_phone_brand_and_brand_model')

add_model('stacking_15',
          XGBClassifier(learning_rate=0.1, max_depth=3, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=70,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: get_model_probs_as_feature([
              'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app',
              'mlp_on_phone_and_installed_app_for_device_with_installed_app',
              'lr_on_installed_and_active_app_for_device_with_installed_app',
              'lr_on_phone_and_installed_and_active_app_for_device_with_installed_app',
              'lr_on_installed_app_ratio_for_device_with_installed_app_2',
              'lr_on_active_app_for_device_with_installed_app'
          ], mask=device_with_event),
          device_with_event, X_filtered=True,
          backoff_model_name='lr_on_phone_brand_and_brand_model')

add_model('stacking_16',
          XGBClassifier(learning_rate=0.1, max_depth=3, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=70,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: get_model_probs_as_feature([
              'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app',
              'mlp_on_phone_and_installed_app_for_device_with_installed_app',
              'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app',
              'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
              'gblinear_on_phone_and_installed_app_for_device_with_installed_app',
              'gblinear_on_phone_and_installed_app_for_device_with_installed_app_2',
              'lr_on_phone_and_installed_app_and_label_for_device_with_installed_app',
              'lr_on_installed_and_active_app_for_device_with_installed_app',
              'lr_on_phone_and_installed_and_active_app_for_device_with_installed_app',
              'lr_on_installed_app_ratio_for_device_with_installed_app_2',
              'lr_on_active_app_for_device_with_installed_app'
          ], mask=device_with_event),
          device_with_event, X_filtered=True,
          backoff_model_name='lr_on_phone_brand_and_brand_model')

add_model('stacking_17',
          XGBClassifier(learning_rate=0.1, max_depth=5, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=70,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: get_model_probs_as_feature([
              'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app',
              'mlp_on_phone_and_installed_app_for_device_with_installed_app',
              'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app',
              'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
              'gblinear_on_phone_and_installed_app_for_device_with_installed_app',
              'gblinear_on_phone_and_installed_app_for_device_with_installed_app_2',
              'lr_on_phone_and_installed_app_and_label_for_device_with_installed_app',
              'lr_on_installed_and_active_app_for_device_with_installed_app',
              'lr_on_phone_and_installed_and_active_app_for_device_with_installed_app',
              'lr_on_installed_app_ratio_for_device_with_installed_app_2',
              'lr_on_active_app_for_device_with_installed_app'
          ], mask=device_with_event),
          device_with_event, X_filtered=True,
          backoff_model_name='lr_on_phone_brand_and_brand_model')

add_model('stacking_18',
          XGBClassifier(learning_rate=0.1, max_depth=5, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=70,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: get_model_probs_as_feature([
              'mlp_on_phone_and_installed_app',
              'mlp_on_phone_and_installed_app_and_label',
              'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app',
              'mlp_on_phone_and_installed_app_for_device_with_installed_app',
              'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app',
              'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
              'gblinear_on_phone_and_installed_app_for_device_with_installed_app',
              'gblinear_on_phone_and_installed_app_for_device_with_installed_app_2',
              'lr_on_phone_and_installed_app_and_label_for_device_with_installed_app',
              'lr_on_installed_and_active_app_for_device_with_installed_app',
              'lr_on_phone_and_installed_and_active_app_for_device_with_installed_app',
              'lr_on_installed_app_ratio_for_device_with_installed_app_2',
              'lr_on_active_app_for_device_with_installed_app'
          ]))

add_model('stacking_19',
          XGBClassifier(learning_rate=0.1, max_depth=5, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=70,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: get_model_probs_as_feature([
              'mlp_on_phone_and_installed_app',
              'mlp_on_phone_and_installed_app_and_label',
              'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app',
              'mlp_on_phone_and_installed_app_for_device_with_installed_app',
          ]))

# full training of stacking_7 ?
add_model('stacking_20',
          XGBClassifier(learning_rate=0.1, max_depth=3, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=70,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: pd.concat((get_full_feats_df(feats_df[['n_event', 'n_event_with_no_app', 'ratio_event_with_no_app',
                                                         'n_app_installed_in_any_event', 'n_active_in_any_event',
                                                         'mean_installed_app_count_in_event',
                                                         'mean_active_app_count_in_event',
                                                         'mean_ratio_of_active_app_in_event',
                                                         'n_event_with_no_active_app',
                                                         'ratio_event_with_no_active_app',
                                                         'mean_app_ratio_of_installed',
                                                         'std_app_ratio_of_installed', 'mean_longitude',
                                                         'mean_latitude', 'var_longitude',
                                                         'var_latitude']]),
                             get_model_probs_as_feature([
                                 'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
                                 'gblinear_on_phone_and_installed_app_for_device_with_installed_app',
                                 'gblinear_on_phone_and_installed_app_for_device_with_installed_app_2',
                                 'lr_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'lr_on_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_phone_and_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_installed_app_ratio_for_device_with_installed_app_2',
                                 'lr_on_active_app_for_device_with_installed_app'
                             ]),), axis=1, ignore_index=True))

add_model('stacking_21',
          XGBClassifier(learning_rate=0.1, max_depth=3, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=70,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: get_model_probs_as_feature([
              'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app',
              'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
              'gblinear_on_phone_and_installed_app_for_device_with_installed_app',
              'gblinear_on_phone_and_installed_app_for_device_with_installed_app_2',
              'lr_on_phone_and_installed_app_and_label_for_device_with_installed_app',
              'lr_on_installed_and_active_app_for_device_with_installed_app',
              'lr_on_phone_and_installed_and_active_app_for_device_with_installed_app',
              'lr_on_installed_app_ratio_for_device_with_installed_app_2',
              'lr_on_active_app_for_device_with_installed_app'
          ]))

add_model('stacking_22',
          XGBClassifier(learning_rate=0.1, max_depth=5, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=70,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: get_model_probs_as_feature([
              'mlp_on_phone_and_installed_app',
              'mlp_on_phone_and_installed_app_and_label',
          ]))

add_model('stacking_23',
          XGBClassifier(learning_rate=0.1, max_depth=5, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=70,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: get_model_probs_as_feature([
              'mlp_on_phone_and_installed_app',
              'mlp_on_phone_and_installed_app_and_label',
              'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app',
              'mlp_on_phone_and_installed_app_for_device_with_installed_app',
              'mlp_on_phone_and_installed_app_2',
              'mlp_on_phone_and_installed_app_and_label_2',
              'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
              'mlp_on_phone_and_installed_app_for_device_with_installed_app_2',
          ]))

add_model('stacking_24',
          XGBClassifier(learning_rate=0.1, max_depth=5, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=70,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: get_model_probs_as_feature([
              'mlp_on_phone_and_installed_app',
              'mlp_on_phone_and_installed_app_and_label',
              'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app',
              'mlp_on_phone_and_installed_app_for_device_with_installed_app',
              'mlp_on_phone_and_installed_app_2',
              'mlp_on_phone_and_installed_app_and_label_2',
              'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
              'mlp_on_phone_and_installed_app_for_device_with_installed_app_2',
              'mlp_on_phone_and_installed_app_3',
              'mlp_on_phone_and_installed_app_and_label_3',
              'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app_3',
              'mlp_on_phone_and_installed_app_for_device_with_installed_app_3',
          ]),
          n_jobs=1)

add_model('stacking_25',
          XGBClassifier(learning_rate=0.1, max_depth=5, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=70,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: get_model_probs_as_feature([
              'mlp_on_phone_and_installed_app',
              'mlp_on_phone_and_installed_app_and_label',
              'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app',
              'mlp_on_phone_and_installed_app_for_device_with_installed_app',
              'mlp_on_phone_and_installed_app_2',
              'mlp_on_phone_and_installed_app_and_label_2',
              'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
              'mlp_on_phone_and_installed_app_for_device_with_installed_app_2',
              'mlp_on_phone_and_installed_app_3',
              'mlp_on_phone_and_installed_app_and_label_3',
              'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app_3',
              'mlp_on_phone_and_installed_app_for_device_with_installed_app_3',
              'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app',
              'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
              'gblinear_on_phone_and_installed_app_for_device_with_installed_app',
              'gblinear_on_phone_and_installed_app_for_device_with_installed_app_2',
              'lr_on_phone_and_installed_app_and_label_for_device_with_installed_app',
              'lr_on_installed_and_active_app_for_device_with_installed_app',
              'lr_on_phone_and_installed_and_active_app_for_device_with_installed_app',
              'lr_on_installed_app_ratio_for_device_with_installed_app_2',
              'lr_on_active_app_for_device_with_installed_app'
          ]),
          n_jobs=1)

add_model('stacking_26',
          XGBClassifier(learning_rate=0.1, max_depth=3, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=70,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: pd.concat((get_full_feats_df(feats_df[['n_event', 'n_event_with_no_app', 'ratio_event_with_no_app',
                                                         'n_app_installed_in_any_event', 'n_active_in_any_event',
                                                         'mean_installed_app_count_in_event',
                                                         'mean_active_app_count_in_event',
                                                         'mean_ratio_of_active_app_in_event',
                                                         'n_event_with_no_active_app',
                                                         'ratio_event_with_no_active_app',
                                                         'mean_app_ratio_of_installed',
                                                         'std_app_ratio_of_installed', 'mean_longitude',
                                                         'mean_latitude', 'var_longitude',
                                                         'var_latitude', 'ratio_of_event_without_valid_location']]),
                             get_model_probs_as_feature([
                                 'mlp_on_phone_and_installed_app',
                                 'mlp_on_phone_and_installed_app_and_label',
                                 'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'mlp_on_phone_and_installed_app_for_device_with_installed_app',
                                 'mlp_on_phone_and_installed_app_2',
                                 'mlp_on_phone_and_installed_app_and_label_2',
                                 'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
                                 'mlp_on_phone_and_installed_app_for_device_with_installed_app_2',
                                 'mlp_on_phone_and_installed_app_3',
                                 'mlp_on_phone_and_installed_app_and_label_3',
                                 'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app_3',
                                 'mlp_on_phone_and_installed_app_for_device_with_installed_app_3',
                                 'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
                                 'gblinear_on_phone_and_installed_app_for_device_with_installed_app',
                                 'gblinear_on_phone_and_installed_app_for_device_with_installed_app_2',
                                 'lr_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'lr_on_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_phone_and_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_installed_app_ratio_for_device_with_installed_app_2',
                                 'lr_on_active_app_for_device_with_installed_app'
                             ]),), axis=1, ignore_index=True))

add_model('stacking_27',
          XGBClassifier(learning_rate=0.05, max_depth=2, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=200,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: pd.concat((get_full_feats_df(feats_df[['n_event', 'n_event_with_no_app', 'ratio_event_with_no_app',
                                                         'n_app_installed_in_any_event', 'n_active_in_any_event',
                                                         'mean_installed_app_count_in_event',
                                                         'mean_active_app_count_in_event',
                                                         'mean_ratio_of_active_app_in_event',
                                                         'n_event_with_no_active_app',
                                                         'ratio_event_with_no_active_app',
                                                         'mean_app_ratio_of_installed',
                                                         'std_app_ratio_of_installed', 'mean_longitude',
                                                         'mean_latitude', 'var_longitude',
                                                         'var_latitude', 'ratio_of_event_without_valid_location']]),
                             get_model_probs_as_feature([
                                 'mlp_on_phone_and_installed_app',
                                 'mlp_on_phone_and_installed_app_and_label',
                                 'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'mlp_on_phone_and_installed_app_for_device_with_installed_app',
                                 'mlp_on_phone_and_installed_app_2',
                                 'mlp_on_phone_and_installed_app_and_label_2',
                                 'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
                                 'mlp_on_phone_and_installed_app_for_device_with_installed_app_2',
                                 'mlp_on_phone_and_installed_app_3',
                                 'mlp_on_phone_and_installed_app_and_label_3',
                                 'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app_3',
                                 'mlp_on_phone_and_installed_app_for_device_with_installed_app_3',
                                 'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
                                 'gblinear_on_phone_and_installed_app_for_device_with_installed_app',
                                 'gblinear_on_phone_and_installed_app_for_device_with_installed_app_2',
                                 'lr_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'lr_on_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_phone_and_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_installed_app_ratio_for_device_with_installed_app_2',
                                 'lr_on_active_app_for_device_with_installed_app'
                             ]),), axis=1, ignore_index=True))

add_model('stacking_28',
          XGBClassifier(learning_rate=0.05, max_depth=2, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=200,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: pd.concat((get_full_feats_df(feats_df[['n_event', 'n_event_with_no_app', 'ratio_event_with_no_app',
                                                         'n_app_installed_in_any_event', 'n_active_in_any_event',
                                                         'mean_installed_app_count_in_event',
                                                         'mean_active_app_count_in_event',
                                                         'mean_ratio_of_active_app_in_event',
                                                         'n_event_with_no_active_app',
                                                         'ratio_event_with_no_active_app',
                                                         'mean_app_ratio_of_installed',
                                                         'std_app_ratio_of_installed', 'mean_longitude',
                                                         'mean_latitude', 'var_longitude',
                                                         'var_latitude', 'ratio_of_event_without_valid_location']]),
                             get_model_probs_as_feature([
                                 'mlp_on_phone_and_installed_app',
                                 'mlp_on_phone_and_installed_app_and_label',
                                 'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'mlp_on_phone_and_installed_app_for_device_with_installed_app',
                                 'mlp_on_phone_and_installed_app_2',
                                 'mlp_on_phone_and_installed_app_and_label_2',
                                 'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
                                 'mlp_on_phone_and_installed_app_for_device_with_installed_app_2',
                                 'mlp_on_phone_and_installed_app_3',
                                 'mlp_on_phone_and_installed_app_and_label_3',
                                 'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app_3',
                                 'mlp_on_phone_and_installed_app_for_device_with_installed_app_3',
                                 'mlp_on_phone_and_installed_app_6',
                                 'mlp_on_phone_and_installed_app_and_label_6',
                                 'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app_6',
                                 'mlp_on_phone_and_installed_app_for_device_with_installed_app_6',
                                 'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
                                 'gblinear_on_phone_and_installed_app_for_device_with_installed_app',
                                 'gblinear_on_phone_and_installed_app_for_device_with_installed_app_2',
                                 'lr_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'lr_on_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_phone_and_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_installed_app_ratio_for_device_with_installed_app_2',
                                 'lr_on_active_app_for_device_with_installed_app'
                             ]),), axis=1, ignore_index=True))

add_model('stacking_29',
          XGBClassifier(learning_rate=0.05, max_depth=2, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=200,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: pd.concat((get_full_feats_df(feats_df[['n_event', 'n_event_with_no_app', 'ratio_event_with_no_app',
                                                         'n_app_installed_in_any_event', 'n_active_in_any_event',
                                                         'mean_installed_app_count_in_event',
                                                         'mean_active_app_count_in_event',
                                                         'mean_ratio_of_active_app_in_event',
                                                         'n_event_with_no_active_app',
                                                         'ratio_event_with_no_active_app',
                                                         'mean_app_ratio_of_installed',
                                                         'std_app_ratio_of_installed', 'mean_longitude',
                                                         'mean_latitude', 'var_longitude',
                                                         'var_latitude', 'ratio_of_event_without_valid_location']]),
                             get_model_probs_as_feature([
                                 'mlp_on_phone_and_installed_app',
                                 'mlp_on_phone_and_installed_app_and_label',
                                 'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'mlp_on_phone_and_installed_app_for_device_with_installed_app',
                                 'mlp_on_phone_and_installed_app_2',
                                 'mlp_on_phone_and_installed_app_and_label_2',
                                 'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
                                 'mlp_on_phone_and_installed_app_for_device_with_installed_app_2',
                                 'mlp_on_phone_and_installed_app_3',
                                 'mlp_on_phone_and_installed_app_and_label_3',
                                 'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app_3',
                                 'mlp_on_phone_and_installed_app_for_device_with_installed_app_3',
                                 'mlp_on_phone_and_installed_app_6',
                                 'mlp_on_phone_and_installed_app_and_label_6',
                                 'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app_6',
                                 'mlp_on_phone_and_installed_app_for_device_with_installed_app_6',
                                 'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
                                 'gblinear_on_phone_and_installed_app_for_device_with_installed_app',
                                 'gblinear_on_phone_and_installed_app_for_device_with_installed_app_2',
                                 'lr_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'lr_on_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_phone_and_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_installed_app_ratio_for_device_with_installed_app_2',
                                 'lr_on_active_app_for_device_with_installed_app',
                                 'lr_on_installed_app_nmf_200_for_device_with_installed_app',
                                 'lr_on_installed_app_svd_200_for_device_with_installed_app',
                                 'lr_on_installed_app_nmf_500_for_device_with_installed_app',
                                 'lr_on_installed_app_svd_500_for_device_with_installed_app',
                             ]),), axis=1, ignore_index=True))

add_model('stacking_30',
          XGBClassifier(learning_rate=0.1, max_depth=3, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=70,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: pd.concat((get_full_feats_df(feats_df[['n_event', 'n_event_with_no_app', 'ratio_event_with_no_app',
                                                         'n_app_installed_in_any_event', 'n_active_in_any_event',
                                                         'mean_installed_app_count_in_event',
                                                         'mean_active_app_count_in_event',
                                                         'mean_ratio_of_active_app_in_event',
                                                         'n_event_with_no_active_app',
                                                         'ratio_event_with_no_active_app',
                                                         'mean_app_ratio_of_installed',
                                                         'std_app_ratio_of_installed', 'mean_longitude',
                                                         'mean_latitude', 'var_longitude',
                                                         'var_latitude', 'ratio_of_event_without_valid_location']]),
                             get_model_probs_as_feature([
                                 'nn1_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'nn1_on_phone_and_installed_app_and_label',
                                 'nn1_on_phone_and_installed_app_for_device_with_installed_app',
                                 'nn1_on_phone_and_installed_app',
                                 'mlp_on_phone_and_installed_app',
                                 'mlp_on_phone_and_installed_app_and_label',
                                 'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'mlp_on_phone_and_installed_app_for_device_with_installed_app',
                                 'mlp_on_phone_and_installed_app_2',
                                 'mlp_on_phone_and_installed_app_and_label_2',
                                 'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
                                 'mlp_on_phone_and_installed_app_for_device_with_installed_app_2',
                                 'mlp_on_phone_and_installed_app_3',
                                 'mlp_on_phone_and_installed_app_and_label_3',
                                 'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app_3',
                                 'mlp_on_phone_and_installed_app_for_device_with_installed_app_3',
                                 'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
                                 'gblinear_on_phone_and_installed_app_for_device_with_installed_app',
                                 'gblinear_on_phone_and_installed_app_for_device_with_installed_app_2',
                                 'lr_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'lr_on_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_phone_and_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_installed_app_ratio_for_device_with_installed_app_2',
                                 'lr_on_active_app_for_device_with_installed_app',
                             ]),), axis=1, ignore_index=True))

add_model('stacking_31',
          XGBClassifier(learning_rate=0.1, max_depth=3, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=70,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: pd.concat((get_full_feats_df(feats_df[['n_event', 'n_event_with_no_app', 'ratio_event_with_no_app',
                                                         'n_app_installed_in_any_event', 'n_active_in_any_event',
                                                         'mean_installed_app_count_in_event',
                                                         'mean_active_app_count_in_event',
                                                         'mean_ratio_of_active_app_in_event',
                                                         'n_event_with_no_active_app',
                                                         'ratio_event_with_no_active_app',
                                                         'mean_app_ratio_of_installed',
                                                         'std_app_ratio_of_installed', 'mean_longitude',
                                                         'mean_latitude', 'var_longitude',
                                                         'var_latitude', 'ratio_of_event_without_valid_location']]),
                             get_model_probs_as_feature([
                                 'nn1_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'nn1_on_phone_and_installed_app_and_label',
                                 'nn1_on_phone_and_installed_app_for_device_with_installed_app',
                                 'nn1_on_phone_and_installed_app',
                                 'mlp_on_phone_and_installed_app',
                                 'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
                                 'gblinear_on_phone_and_installed_app_for_device_with_installed_app',
                                 'gblinear_on_phone_and_installed_app_for_device_with_installed_app_2',
                                 'lr_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'lr_on_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_phone_and_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_installed_app_ratio_for_device_with_installed_app_2',
                                 'lr_on_active_app_for_device_with_installed_app',
                             ]),), axis=1, ignore_index=True))

add_model('stacking_32',
          XGBClassifier(learning_rate=0.1, max_depth=3, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=70,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: pd.concat((get_full_feats_df(feats_df[['n_event', 'n_event_with_no_app', 'ratio_event_with_no_app',
                                                         'n_app_installed_in_any_event', 'n_active_in_any_event',
                                                         'mean_installed_app_count_in_event',
                                                         'mean_active_app_count_in_event',
                                                         'mean_ratio_of_active_app_in_event',
                                                         'n_event_with_no_active_app',
                                                         'ratio_event_with_no_active_app',
                                                         'mean_app_ratio_of_installed',
                                                         'std_app_ratio_of_installed', 'mean_longitude',
                                                         'mean_latitude', 'var_longitude',
                                                         'var_latitude', 'ratio_of_event_without_valid_location']]),
                             get_model_probs_as_feature([
                                 'nn1_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
                                 'nn1_on_phone_and_installed_app_and_label_2',
                                 'nn1_on_phone_and_installed_app_for_device_with_installed_app_2',
                                 'nn1_on_phone_and_installed_app_2',
                                 'mlp_on_phone_and_installed_app',
                                 'mlp_on_phone_and_installed_app_and_label',
                                 'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'mlp_on_phone_and_installed_app_for_device_with_installed_app',
                                 'mlp_on_phone_and_installed_app_2',
                                 'mlp_on_phone_and_installed_app_and_label_2',
                                 'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
                                 'mlp_on_phone_and_installed_app_for_device_with_installed_app_2',
                                 'mlp_on_phone_and_installed_app_3',
                                 'mlp_on_phone_and_installed_app_and_label_3',
                                 'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app_3',
                                 'mlp_on_phone_and_installed_app_for_device_with_installed_app_3',
                                 'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
                                 'gblinear_on_phone_and_installed_app_for_device_with_installed_app',
                                 'gblinear_on_phone_and_installed_app_for_device_with_installed_app_2',
                                 'lr_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'lr_on_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_phone_and_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_installed_app_ratio_for_device_with_installed_app_2',
                                 'lr_on_active_app_for_device_with_installed_app',
                             ]),), axis=1, ignore_index=True))

add_model('stacking_33',
          XGBClassifier(learning_rate=0.1, max_depth=2, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=200,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: pd.concat((get_full_feats_df(feats_df[['n_event', 'n_event_with_no_app', 'ratio_event_with_no_app',
                                                         'n_app_installed_in_any_event', 'n_active_in_any_event',
                                                         'mean_installed_app_count_in_event',
                                                         'mean_active_app_count_in_event',
                                                         'mean_ratio_of_active_app_in_event',
                                                         'n_event_with_no_active_app',
                                                         'ratio_event_with_no_active_app',
                                                         'mean_app_ratio_of_installed',
                                                         'std_app_ratio_of_installed', 'mean_longitude',
                                                         'mean_latitude', 'var_longitude',
                                                         'var_latitude', 'ratio_of_event_without_valid_location']]),
                             get_model_probs_as_feature([
                                 'nn1_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
                                 'nn1_on_phone_and_installed_app_and_label_2',
                                 'nn1_on_phone_and_installed_app_for_device_with_installed_app_2',
                                 'nn1_on_phone_and_installed_app_2',
                                 'mlp_on_phone_and_installed_app',
                                 'mlp_on_phone_and_installed_app_and_label',
                                 'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'mlp_on_phone_and_installed_app_for_device_with_installed_app',
                                 'mlp_on_phone_and_installed_app_2',
                                 'mlp_on_phone_and_installed_app_and_label_2',
                                 'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
                                 'mlp_on_phone_and_installed_app_for_device_with_installed_app_2',
                                 'mlp_on_phone_and_installed_app_3',
                                 'mlp_on_phone_and_installed_app_and_label_3',
                                 'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app_3',
                                 'mlp_on_phone_and_installed_app_for_device_with_installed_app_3',
                                 'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
                                 'gblinear_on_phone_and_installed_app_for_device_with_installed_app',
                                 'gblinear_on_phone_and_installed_app_for_device_with_installed_app_2',
                                 'lr_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'lr_on_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_phone_and_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_installed_app_ratio_for_device_with_installed_app_2',
                                 'lr_on_active_app_for_device_with_installed_app',
                             ]),), axis=1, ignore_index=True))

add_model('stacking_34',
          XGBClassifier(learning_rate=0.1, max_depth=3, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=70,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: pd.concat((get_full_feats_df(feats_df[['n_event', 'n_event_with_no_app', 'ratio_event_with_no_app',
                                                         'n_app_installed_in_any_event', 'n_active_in_any_event',
                                                         'mean_installed_app_count_in_event',
                                                         'mean_active_app_count_in_event',
                                                         'mean_ratio_of_active_app_in_event',
                                                         'n_event_with_no_active_app',
                                                         'ratio_event_with_no_active_app',
                                                         'mean_app_ratio_of_installed',
                                                         'std_app_ratio_of_installed', 'mean_longitude',
                                                         'mean_latitude', 'var_longitude',
                                                         'var_latitude', 'ratio_of_event_without_valid_location']]),
                             get_model_probs_as_feature([
                                 'nn1_on_phone_and_installed_app_and_label_for_device_with_installed_app_3',
                                 'nn1_on_phone_and_installed_app_and_label_3',
                                 'nn1_on_phone_and_installed_app_for_device_with_installed_app_3',
                                 'nn1_on_phone_and_installed_app_3',
                                 'mlp_on_phone_and_installed_app',
                                 'mlp_on_phone_and_installed_app_for_device_with_installed_app',
                                 'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
                                 'gblinear_on_phone_and_installed_app_for_device_with_installed_app',
                                 'gblinear_on_phone_and_installed_app_for_device_with_installed_app_2',
                             ]),), axis=1, ignore_index=True))

add_model('stacking_35',
          XGBClassifier(learning_rate=0.1, max_depth=3, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=70,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: get_model_probs_as_feature([
              'nn1_on_phone_and_installed_app_and_label_for_device_with_installed_app_3',
              'nn1_on_phone_and_installed_app_and_label_3',
              'nn1_on_phone_and_installed_app_for_device_with_installed_app_3',
              'nn1_on_phone_and_installed_app_3',
              'mlp_on_phone_and_installed_app',
              'mlp_on_phone_and_installed_app_for_device_with_installed_app',
              'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app',
              'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
              'gblinear_on_phone_and_installed_app_for_device_with_installed_app',
              'gblinear_on_phone_and_installed_app_for_device_with_installed_app_2',
          ]))

add_model('stacking_36',
          make_pipeline(Imputer(strategy='mean'), LogisticRegressionCV(solver='lbfgs', n_jobs=5,
                                             scoring='log_loss')),
          lambda: get_model_probs_as_feature([
              'nn1_on_phone_and_installed_app_and_label_for_device_with_installed_app_3',
              'nn1_on_phone_and_installed_app_and_label_3',
              'nn1_on_phone_and_installed_app_for_device_with_installed_app_3',
              'nn1_on_phone_and_installed_app_3',
              'mlp_on_phone_and_installed_app',
              'mlp_on_phone_and_installed_app_for_device_with_installed_app',
              'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app',
              'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
              'gblinear_on_phone_and_installed_app_for_device_with_installed_app',
              'gblinear_on_phone_and_installed_app_for_device_with_installed_app_2',
          ]))


add_model('stacking_37',
          XGBClassifier(learning_rate=0.1, max_depth=3, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=70,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: pd.concat((get_full_feats_df(feats_df[['n_event', 'n_event_with_no_app', 'ratio_event_with_no_app',
                                                         'n_app_installed_in_any_event', 'n_active_in_any_event',
                                                         'mean_installed_app_count_in_event',
                                                         'mean_active_app_count_in_event',
                                                         'mean_ratio_of_active_app_in_event',
                                                         'n_event_with_no_active_app',
                                                         'ratio_event_with_no_active_app',
                                                         'mean_app_ratio_of_installed',
                                                         'std_app_ratio_of_installed', 'mean_longitude',
                                                         'mean_latitude', 'var_longitude',
                                                         'var_latitude', 'ratio_of_event_without_valid_location']]),
                             get_model_probs_as_feature([
                                 'nn1_on_phone_and_installed_app_and_label_for_device_with_installed_app_3',
                                 'nn1_on_phone_and_installed_app_and_label_3',
                                 'nn1_on_phone_and_installed_app_for_device_with_installed_app_3',
                                 'nn1_on_phone_and_installed_app_3',
                                 'mlp_on_phone_and_installed_app',
                                 'mlp_on_phone_and_installed_app_and_label',
                                 'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'mlp_on_phone_and_installed_app_for_device_with_installed_app',
                                 'mlp_on_phone_and_installed_app_2',
                                 'mlp_on_phone_and_installed_app_and_label_2',
                                 'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
                                 'mlp_on_phone_and_installed_app_for_device_with_installed_app_2',
                                 'mlp_on_phone_and_installed_app_3',
                                 'mlp_on_phone_and_installed_app_and_label_3',
                                 'mlp_on_phone_and_installed_app_and_label_for_device_with_installed_app_3',
                                 'mlp_on_phone_and_installed_app_for_device_with_installed_app_3',
                                 'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
                                 'gblinear_on_phone_and_installed_app_for_device_with_installed_app',
                                 'gblinear_on_phone_and_installed_app_for_device_with_installed_app_2',
                                 'lr_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'lr_on_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_phone_and_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_installed_app_ratio_for_device_with_installed_app_2',
                                 'lr_on_active_app_for_device_with_installed_app',
                             ]),), axis=1, ignore_index=True))


add_model('stacking_38',
          XGBClassifier(learning_rate=0.1, max_depth=3, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=70,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: get_model_probs_as_feature([
              'lr_on_phone_brand_and_brand_model',
              'lr_on_phone_and_installed_app_and_label_for_device_with_installed_app',
              'nn2_on_phone_and_installed_app_and_label_for_device_with_installed_app',
              'nn2_on_phone_and_installed_app_and_label',
              'nn2_on_phone_and_installed_app_for_device_with_installed_app',
              'nn2_on_phone_and_installed_app',
              'nn1_on_phone_and_installed_app_and_label_for_device_with_installed_app_3',
              'nn1_on_phone_and_installed_app_and_label_3',
              'nn1_on_phone_and_installed_app_for_device_with_installed_app_3',
              'nn1_on_phone_and_installed_app_3',
              'nn1_on_installed_app_and_label_for_device_with_installed_app_3',
              'nn1_on_installed_app_for_device_with_installed_app_3',
              'nn1_on_installed_app_label_for_device_with_installed_app_3',
              'nn1_on_installed_app_category_for_device_with_installed_app_3',
              'nn1_on_installed_app_ratio_for_device_with_installed_app_3',
          ]))

add_model('stacking_39',
          XGBClassifier(learning_rate=0.1, max_depth=3, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=70,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: get_model_probs_as_feature([
              'nn3_on_phone_for_device_with_installed_app',
              'nn3_on_phone',
              'lr_on_phone_brand_and_brand_model',
              'lr_on_phone_and_installed_app_and_label_for_device_with_installed_app',
              'nn2_on_phone_and_installed_app_and_label_for_device_with_installed_app',
              'nn2_on_phone_and_installed_app_and_label',
              'nn2_on_phone_and_installed_app_for_device_with_installed_app',
              'nn2_on_phone_and_installed_app',
              'nn1_on_phone_and_installed_app_and_label_for_device_with_installed_app_3',
              'nn1_on_phone_and_installed_app_and_label_3',
              'nn1_on_phone_and_installed_app_for_device_with_installed_app_3',
              'nn1_on_phone_and_installed_app_3',
              'nn1_on_installed_app_and_label_for_device_with_installed_app_3',
              'nn1_on_installed_app_for_device_with_installed_app_3',
              'nn1_on_installed_app_label_for_device_with_installed_app_3',
              'nn1_on_installed_app_category_for_device_with_installed_app_3',
              'nn1_on_installed_app_ratio_for_device_with_installed_app_3',
          ]))

add_model('stacking_40',
          XGBClassifier(learning_rate=0.1, max_depth=3, subsample=0.8, colsample_bytree=0.8,
                        n_estimators=70,
                        min_child_weight=10, objective='multi:softprob', seed=1),
          lambda: pd.concat((get_full_feats_df(feats_df[['n_event', 'n_event_with_no_app', 'ratio_event_with_no_app',
                                                         'n_app_installed_in_any_event', 'n_active_in_any_event',
                                                         'mean_installed_app_count_in_event',
                                                         'mean_active_app_count_in_event',
                                                         'mean_ratio_of_active_app_in_event',
                                                         'n_event_with_no_active_app',
                                                         'ratio_event_with_no_active_app',
                                                         'mean_app_ratio_of_installed',
                                                         'std_app_ratio_of_installed', 'mean_longitude',
                                                         'mean_latitude', 'var_longitude',
                                                         'var_latitude', 'ratio_of_event_without_valid_location']]),
                             get_model_probs_as_feature([
                                 'nn3_on_phone_for_device_with_installed_app',
                                 'nn3_on_phone',
                                 'lr_on_phone_brand_and_brand_model',
                                 'lr_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'nn2_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'nn2_on_phone_and_installed_app_and_label',
                                 'nn2_on_phone_and_installed_app_for_device_with_installed_app',
                                 'nn2_on_phone_and_installed_app',
                                 'nn1_on_phone_and_installed_app_and_label_for_device_with_installed_app_3',
                                 'nn1_on_phone_and_installed_app_and_label_3',
                                 'nn1_on_phone_and_installed_app_for_device_with_installed_app_3',
                                 'nn1_on_phone_and_installed_app_3',
                                 'nn1_on_installed_app_and_label_for_device_with_installed_app_3',
                                 'nn1_on_installed_app_for_device_with_installed_app_3',
                                 'nn1_on_installed_app_label_for_device_with_installed_app_3',
                                 'nn1_on_installed_app_category_for_device_with_installed_app_3',
                                 'nn1_on_installed_app_ratio_for_device_with_installed_app_3',
                                 'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'gblinear_on_phone_and_installed_app_and_label_for_device_with_installed_app_2',
                                 'gblinear_on_phone_and_installed_app_for_device_with_installed_app',
                                 'gblinear_on_phone_and_installed_app_for_device_with_installed_app_2',
                                 'lr_on_phone_and_installed_app_and_label_for_device_with_installed_app',
                                 'lr_on_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_phone_and_installed_and_active_app_for_device_with_installed_app',
                                 'lr_on_installed_app_ratio_for_device_with_installed_app_2',
                                 'lr_on_active_app_for_device_with_installed_app',
                             ]),), axis=1, ignore_index=True))